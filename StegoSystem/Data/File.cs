﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.Data
{
    public class File : IData
    {
        private String fName;
        private FileStream fStream;
        private FileHeader fHeader;

        public File(String fName)
        {
            this.fName = fName;
            fHeader = new FileHeader();
            String extension = System.IO.Path.GetExtension(fName).Substring(1); // GetExtension возвращает .txt
            
            TypeFile typeFile = TypeFile.GetFileType(extension);
            fHeader.SetProperty(HeaderProperty.TYPE_FILE, typeFile);
            fStream = null;
        }
        public int GetSize()
        {
            //using (Stream s = new FileStream(fName, FileMode.Open))
            //{
            //    return (int)s.Length;
            //}

            System.IO.FileInfo info = new System.IO.FileInfo(fName);
            return (int)info.Length;
        }
        public byte[] GetBytes()
        {
            //this.GetStream();
            //int length = (int)fStream.Length;
            //byte[] bytes = new byte[length];
            //if(fStream.Read(bytes, 0, length) == length)
            //    return bytes;
            //return null;

            return System.IO.File.ReadAllBytes(fName);
        }

        public IHeader GetHeader()
        {
            return fHeader;
        }

        public Stream GetStream()
        {
            if(fStream == null)
            {
                fStream = new FileStream(fName, FileMode.Open, FileAccess.Read);
                //fHeader.SetProperty(HeaderProperty.SIZE_FILE, (uint)fStream.Length);
            }

            return fStream;
        }
    }
}
