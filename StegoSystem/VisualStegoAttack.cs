﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem
{
    public class VisualStegoAttack
    {
        public static Bitmap ViewBit(Bitmap srcImg, int mask)
        {
            Bitmap dstImg = new Bitmap(srcImg.Width, srcImg.Height);
            Color color;

            for (int y = 0; y < srcImg.Height; ++y)
            {
                for (int x = 0; x < srcImg.Width; ++x)
                {
                    color = srcImg.GetPixel(x, y);
                    dstImg.SetPixel(x, y, Color.FromArgb((color.R & mask) == mask ? (byte)255 : (byte)0,
                                                         (color.G & mask) == mask ? (byte)255 : (byte)0,
                                                         (color.B & mask) == mask ? (byte)255 : (byte)0));
                }
            }

            return dstImg;
        }
    }
}
