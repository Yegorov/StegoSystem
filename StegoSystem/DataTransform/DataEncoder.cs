﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StegoSystem.Data;

namespace StegoSystem.DataTransform
{
    public class DataEncoder : IDataTransformEncoder
    {
        private StegoKey stegoKey;
        private IHeader header;

        public DataEncoder(StegoKey stegoKey)
        {
            this.SetStegoKey(stegoKey);
        }

        public IHeader GetHeader()
        {
            return header;
        }

        public void SetStegoKey(StegoKey stegoKey)
        {
            this.stegoKey = stegoKey;
        }
        public byte[] TransfromFromBytes(byte[] bytes)
        {
            ISymmetricEncryption decryption = SymmetricEncryptionFactory.CreateFromNumber(stegoKey.TypeEncryption);
            decryption.SetKeyIV(stegoKey.Key, stegoKey.IV);
            byte[] decryptedBytes = decryption.GetBytes(bytes, DataMode.OUT);

            byte[] headerBytes = new byte[decryptedBytes[1]]; // Второй байт размер заголовка (смещение данных от начала)
            byte[] dataBytes = new byte[decryptedBytes.Length - headerBytes.Length];
            Array.Copy(decryptedBytes, 0, headerBytes, 0, headerBytes.Length);
            Array.Copy(decryptedBytes, decryptedBytes[1], dataBytes, 0, dataBytes.Length);

            header = HeaderFactory.CreateFromBytes(headerBytes);

            ICompression compression = CompressionFactory.CreateFromNumber((byte)((CompressMode)header.GetProperty(HeaderProperty.COMPRESSION_MODE)).Value);
            IHashSum hashSum = HashSumFactory.CreateFromNumber((byte)((TypeHashSum)header.GetProperty(HeaderProperty.TYPE_HASH_SUM)).Value);

            byte[] decompressBytes = compression.GetBytes(dataBytes, DataMode.OUT);

            byte[] calcHash = hashSum.GetHashBytes(decompressBytes);

            if(calcHash != null)
                if(!Enumerable.SequenceEqual(calcHash, (byte[])header.GetProperty(HeaderProperty.VALUE_HASH_SUM)))
                    throw new InvalidOperationException("Не совпадает вычисленная хеш-сумма данных с суммой в заголовке");

            return decompressBytes;
        }
        public Stream TransformFromStream(Stream stream)
        {
            throw new NotImplementedException();
        }
    }

    public static class SymmetricEncryptionFactory
    {
        public static ISymmetricEncryption CreateFromNumber(byte n)
        {
            TypeEncryptionValue val = (TypeEncryptionValue)n;

            if (val == TypeEncryptionValue.NONE)
                return new NoneEncryption();
            else if (val == TypeEncryptionValue.AES_128)
                return new AES128Encryption();
            else if (val == TypeEncryptionValue.TRIPLE_DES_128)
                return new TripleDES128Encryption();

            throw new Exception("Не найден алгоритм для шифрования/дешифрования данных");
        }
        public static ISymmetricEncryption CreateFromString(String nameEnc)
        {
            if (nameEnc == TypeEncryptionValueConst.NONE)
                return new NoneEncryption();
            else if (nameEnc == TypeEncryptionValueConst.AES_128)
                return new AES128Encryption();
            else if (nameEnc == TypeEncryptionValueConst.TRIPLE_DES_128)
                return new TripleDES128Encryption();

            throw new Exception("Не найден алгоритм для шифрования/дешифрования данных");
        }
    }

    public static class CompressionFactory
    {
        public static ICompression CreateFromNumber(byte n)
        {
            if (n == CompressMode.NONE.Value)
                return new NoneCompression();
            else if (n == CompressMode.GZIP.Value)
                return new GZipCompression();
            else if (n == CompressMode.DEFLATE.Value)
                return new DeflateCompression();

            throw new Exception("Не найден алгоритм для сжатия/разжатия данных");
        }
        public static ICompression CreateFromString(String nameCompr)
        {
            if (nameCompr == CompressMode.NONE.Name)
                return new NoneCompression();
            else if (nameCompr == CompressMode.GZIP.Name)
                return new GZipCompression();
            else if (nameCompr == CompressMode.DEFLATE.Name)
                return new DeflateCompression();

            throw new Exception("Не найден алгоритм для сжатия/разжатия данных");
        }
    }

    public static class HashSumFactory
    {
        public static IHashSum CreateFromNumber(byte n)
        {
            if (n == TypeHashSum.NONE.Value)
                return new NoneHashSum();
            else if (n == TypeHashSum.MD5.Value)
                return new MD5HashSum();
            else if (n == TypeHashSum.SHA_1.Value)
                return new SHA_1HashSum();
            else if (n == TypeHashSum.SHA_256.Value)
                return new SHA_256HashSum();

            throw new Exception("Не найден алгоритм для вычисления контрольной суммы данных");
        }

        public static IHashSum CreateFromString(String nameHash)
        {
            if (nameHash == TypeHashSum.NONE.Name)
                return new NoneHashSum();
            else if (nameHash == TypeHashSum.MD5.Name)
                return new MD5HashSum();
            else if (nameHash == TypeHashSum.SHA_1.Name)
                return new SHA_1HashSum();
            else if (nameHash == TypeHashSum.SHA_256.Name)
                return new SHA_256HashSum();

            throw new Exception("Не найден алгоритм для вычисления контрольной суммы данных");
        }

    }
}
