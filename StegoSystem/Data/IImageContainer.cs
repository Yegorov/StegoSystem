﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem
{
    public interface IImageContainer
    {
        uint Height { get; }
        uint Width { get; }
    }
}
