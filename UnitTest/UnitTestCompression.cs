﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StegoSystem.DataTransform;
using System.Text;
using System.IO;

namespace UnitTest
{
    [TestClass]
    public class UnitTestCompression
    {
        [TestMethod]
        public void TestAllCompression()
        {
            this.TestCompression(new NoneCompression());
            this.TestCompression(new GZipCompression());
            this.TestCompression(new DeflateCompression());
        }
        public void TestCompression(ICompression c)
        {
            ICompression compress = c;
            String inS = "Кардинальное различие между ними в том, что сжатие без потерь обеспечивает возможность точного восстановления исходного сообщения. Сжатие с потерями же позволяет получить только некоторое приближение исходного сообщения, то есть отличающееся от исходного, но в пределах некоторых заранее определённых погрешностей.";
            byte[] bIn = Encoding.UTF8.GetBytes(inS);
            int sizeBefore = bIn.Length;

            byte[] bOut = compress.GetBytes(bIn, DataMode.IN);
            int sizeAfter = bOut.Length;

            byte[] bNew = compress.GetBytes(bOut, DataMode.OUT);

            String outS = Encoding.UTF8.GetString(bNew);

            CollectionAssert.AreEqual(bIn, bNew);
            StringAssert.Equals(inS, outS);

            MemoryStream inms = new MemoryStream(bIn);
            MemoryStream outms = new MemoryStream();
            MemoryStream out2ms = new MemoryStream();
            Stream s = compress.GetStream(inms, outms, DataMode.IN);

            s.Close(); // Zip Stream close / Start Compress
            byte[] msbyte = outms.ToArray(); // Get Compress Bytes
            MemoryStream in2ms = new MemoryStream(msbyte);

            compress.GetStream(in2ms, out2ms, DataMode.OUT);
            byte[] ms2byte = out2ms.ToArray();

            inms.Close();
            in2ms.Close();
            outms.Close();
            out2ms.Close();

            CollectionAssert.AreEqual(bOut, msbyte);
            CollectionAssert.AreEqual(bIn, ms2byte);
        }
    }
}
