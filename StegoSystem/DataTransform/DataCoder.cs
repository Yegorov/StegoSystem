﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StegoSystem.Data;

namespace StegoSystem.DataTransform
{
    public class DataCoder : IDataTransformCoder
    {
        private StegoKey stegoKey;
        private IHashSum hashSum;
        private ICompression compress;
        private ISymmetricEncryption encryption;

        public DataCoder(IHashSum hashSum, ICompression compress, ISymmetricEncryption encryption)
        {
            stegoKey = new StegoKey();

            this.hashSum = hashSum;
            if (hashSum == null)
                this.hashSum = new NoneHashSum();

            this.compress = compress;
            if (compress == null)
                this.compress = new NoneCompression();

            this.encryption = encryption;
            if (encryption == null)
                this.encryption = new NoneEncryption();
        }
        public StegoKey GetStegoKey()
        {
            return stegoKey;
        }
        public byte[] TransfromToBytes(IData data)
        {
            byte[] srcBytes = data.GetBytes();
            data.GetHeader().SetProperty(HeaderProperty.SIZE_FILE, (uint)srcBytes.Length);
            
            // Вычисляем хеш
            byte[] hashSumBytes = hashSum.GetHashBytes(srcBytes);
            String nameHashSum = hashSum.GetHashName();
            data.GetHeader().SetProperty(HeaderProperty.TYPE_HASH_SUM, TypeHashSum.GetTypeHashSum(nameHashSum));
            data.GetHeader().SetProperty(HeaderProperty.VALUE_HASH_SUM, hashSumBytes);
            
            // Сжимаем данные
            byte[] compressBytes = compress.GetBytes(srcBytes, DataMode.IN);
            String nameCompress = compress.GetCompressName();
            data.GetHeader().SetProperty(HeaderProperty.COMPRESSION_MODE, CompressMode.GetCompressMode(nameCompress));
            
            // Шифрование происходит вместе с заголовком (его необходимо склеить с основными данными)
            // Шифруем данные
            byte[] headerBytes = data.GetHeader().GetBytes();
            byte[] allBytesWithHeader = new byte[headerBytes.Length + compressBytes.Length];
            Array.Copy(headerBytes, 0, allBytesWithHeader, 0, headerBytes.Length);
            Array.Copy(compressBytes, 0, allBytesWithHeader, headerBytes.Length, compressBytes.Length);

            byte[] encryptBytes = encryption.GetBytes(allBytesWithHeader, DataMode.IN);
            byte[] iv = encryption.GetIV();
            byte[] key = encryption.GetKey();

            stegoKey.TypeEncryption = (byte)encryption.GetTypeEncryptionValue();
            stegoKey.SetIV(iv);
            stegoKey.SetKey(key);

            return encryptBytes;
        }
        public Stream TransformToStream(IData data)
        {
            throw new NotImplementedException();
        }
    }
}
