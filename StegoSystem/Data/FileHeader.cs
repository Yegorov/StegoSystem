﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.Data
{
    public class FileHeader : IHeader
    {
        private TypeFile typeFile; // 1 byte
        private byte offsetData;  // 1 byte
        private uint sizeFile; // 4 byte

        private CompressMode compressMode; // 1 byte

        private TypeHashSum typeHashSum; // 1 byte
        private byte[] valueHashSum; // n byte

        // Размеры полей в байтах
        private static readonly int SIZEOF_TYPE_FILE = 1;
        private static readonly int SIZEOF_OFFSET_DATA = 1;
        private static readonly int SIZEOF_SIZE_FILE = 4;
        private static readonly int SIZEOF_COMPRESS_MODE = 1;
        private static readonly int SIZEOF_TYPE_HASH_SUM = 1;

        public FileHeader()
        {
            typeFile = TypeFile.UNKNOWN;
            offsetData = (byte)GetOnlyConstSize();
            sizeFile = 0;
            compressMode = CompressMode.NONE;
            typeHashSum = TypeHashSum.NONE;
            valueHashSum = null;
        }

        public static FileHeader CreateFileHeaderFromBytes(byte[] bytes)
        {
            FileHeader fh = new FileHeader();

            fh.SetProperty(HeaderProperty.TYPE_FILE, TypeFile.GetFileType(bytes[0]));
            fh.SetProperty(HeaderProperty.OFFSET_DATA, bytes[1]);
            fh.SetProperty(HeaderProperty.SIZE_FILE, BitConverter.ToUInt32(bytes.Skip(2).Take(SIZEOF_SIZE_FILE).ToArray(), 0));
            fh.SetProperty(HeaderProperty.COMPRESSION_MODE, CompressMode.GetCompressMode(bytes[6]));

            TypeHashSum tHashSum = TypeHashSum.GetTypeHashSum(bytes[7]);
            fh.SetProperty(HeaderProperty.TYPE_HASH_SUM, tHashSum);
            if (tHashSum != TypeHashSum.NONE)
            {
                byte[] hashSum = bytes.Skip(8).Take(tHashSum.SizeInBytes).ToArray();
                fh.SetProperty(HeaderProperty.VALUE_HASH_SUM, hashSum);
            }

            return fh;
        }

        public int GetOnlyConstSize()
        {
            return SIZEOF_TYPE_FILE + SIZEOF_OFFSET_DATA + SIZEOF_SIZE_FILE + SIZEOF_COMPRESS_MODE + SIZEOF_TYPE_HASH_SUM;
        }

        public byte[] GetBytes()
        {
            int allSizeBytes = this.GetOnlyConstSize();
            byte[] bytes = new byte[allSizeBytes + typeHashSum.SizeInBytes];

            bytes[0] = (byte)typeFile.Value; // 0
            bytes[1] = (byte)(allSizeBytes + typeHashSum.SizeInBytes); // 1
            Array.Copy(BitConverter.GetBytes(sizeFile), 0, bytes, 2, SIZEOF_SIZE_FILE); // 2, 3, 4, 5
            bytes[6] = (byte)compressMode.Value; // 6
            bytes[7] = (byte)typeHashSum.Value; // 7
            
            if(typeHashSum != TypeHashSum.NONE)
                Array.Copy(valueHashSum, 0, bytes, 8, typeHashSum.SizeInBytes); // 8 ... 8 + typeHashSum.Size - 8

            return bytes;
        }

        public object GetProperty(HeaderProperty property)
        {
            switch (property)
            {
                case HeaderProperty.TYPE_FILE:
                    {
                        return typeFile;
                        break;
                    }
                case HeaderProperty.OFFSET_DATA:
                    {
                        return offsetData;
                        break;
                    }
                case HeaderProperty.SIZE_FILE:
                    {
                        return sizeFile;
                        break;
                    }
                case HeaderProperty.COMPRESSION_MODE:
                    {
                        return compressMode;
                        break;
                    }
                case HeaderProperty.TYPE_HASH_SUM:
                    {
                        return typeHashSum;
                        break;
                    }
                case HeaderProperty.VALUE_HASH_SUM:
                    {
                        return valueHashSum;
                        break;
                    }
            }
            return null;
        }

        public void SetProperty(HeaderProperty property, object value)
        {
            switch(property)
            {
                case HeaderProperty.TYPE_FILE:
                    {
                        if (((TypeFile)value) == TypeFile.NONE)
                            typeFile = TypeFile.UNKNOWN;
                        else
                            typeFile = (TypeFile)value;
                        break;
                    }
                case HeaderProperty.OFFSET_DATA:
                    {
                        offsetData = (byte)value;
                        break;
                    }
                case HeaderProperty.SIZE_FILE:
                    {
                        sizeFile = (uint)value;
                        break;
                    }
                case HeaderProperty.COMPRESSION_MODE:
                    {
                        compressMode = (CompressMode)value;
                        break;
                    }
                case HeaderProperty.TYPE_HASH_SUM:
                    {
                        typeHashSum = (TypeHashSum)value;
                        break;
                    }
                case HeaderProperty.VALUE_HASH_SUM:
                    {
                        valueHashSum = (byte[])value;
                        break;
                    }
            }
        }
    }
}
