﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.DataTransform
{
    public class NoneCompression : ICompression
    {
        private readonly String compressName = "";

        public byte[] GetBytes(byte[] bytes, DataMode mode)
        {
            return bytes;
        }

        public string GetCompressName()
        {
            return compressName;
        }

        public Stream GetStream(Stream dataStream, Stream outStream, DataMode mode)
        {
            dataStream.CopyTo(outStream);
            return outStream;
        }
    }

    public class GZipCompression : ICompression
    {
        // http://stackoverflow.com/questions/3722192/how-do-i-use-gzipstream-with-system-io-memorystream
        // http://snipd.net/gzip-compression-and-decompression-in-c
        private readonly String compressName = "gzip";
        public byte[] GetBytes(byte[] bytes, DataMode mode)
        {
            byte[] outBytes;
            if (mode == DataMode.IN)
            {
                using (MemoryStream mCompressStream = new MemoryStream())
                {
                    using (GZipStream gzipStream = new GZipStream(mCompressStream, CompressionMode.Compress))
                        using (MemoryStream mOriginStream = new MemoryStream(bytes))
                            mOriginStream.CopyTo(gzipStream);

                    outBytes = mCompressStream.ToArray();
                }
            }
            else
            {
                using (MemoryStream mOriginStream = new MemoryStream(bytes))
                {
                    using (GZipStream gzipStream = new GZipStream(mOriginStream, CompressionMode.Decompress))
                    {
                        using (MemoryStream mDecompressStream = new MemoryStream())
                        {
                            gzipStream.CopyTo(mDecompressStream);
                            outBytes = mDecompressStream.ToArray();
                        }
                    }
                }
            }

            return outBytes;
        }

        public Stream GetStream(Stream dataStream, Stream outStream, DataMode mode)
        {
            if (mode == DataMode.IN)
            {
                GZipStream gzipStream = new GZipStream(outStream, CompressionMode.Compress);
                dataStream.CopyTo(gzipStream);
                //gzipStream.Close();
                // or
                return gzipStream;
            }
            else
            {
                GZipStream gzipStream = new GZipStream(dataStream, CompressionMode.Decompress);
                gzipStream.CopyTo(outStream);
                //gzipStream.Close();
                // or
                return gzipStream;
            }
        }

        public string GetCompressName()
        {
            return compressName;
        }
    }

    public class DeflateCompression : ICompression
    {
        private readonly String compressName = "deflate";
        public byte[] GetBytes(byte[] bytes, DataMode mode)
        {
            byte[] outBytes;
            if (mode == DataMode.IN)
            {
                using (MemoryStream mCompressStream = new MemoryStream())
                {
                    using (DeflateStream deflateStream = new DeflateStream(mCompressStream, CompressionMode.Compress))
                    using (MemoryStream mOriginStream = new MemoryStream(bytes))
                        mOriginStream.CopyTo(deflateStream);

                    outBytes = mCompressStream.ToArray();
                }
            }
            else
            {
                using (MemoryStream mOriginStream = new MemoryStream(bytes))
                {
                    using (DeflateStream deflateStream = new DeflateStream(mOriginStream, CompressionMode.Decompress))
                    {
                        using (MemoryStream mDecompressStream = new MemoryStream())
                        {
                            deflateStream.CopyTo(mDecompressStream);
                            outBytes = mDecompressStream.ToArray();
                        }
                    }
                }
            }

            return outBytes;
        }

        public Stream GetStream(Stream dataStream, Stream outStream, DataMode mode)
        {
            if (mode == DataMode.IN)
            {
                DeflateStream deflateStream = new DeflateStream(outStream, CompressionMode.Compress);
                dataStream.CopyTo(deflateStream);
                //deflateStream.Close();
                // or
                return deflateStream;
            }
            else
            {
                DeflateStream deflateStream = new DeflateStream(dataStream, CompressionMode.Decompress);
                deflateStream.CopyTo(outStream);
                //deflateStream.Close();
                // or
                return deflateStream;
            }
        }

        public string GetCompressName()
        {
            return compressName;
        }
    }
}
