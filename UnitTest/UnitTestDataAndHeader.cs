﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StegoSystem.Data;
using System.Text;

namespace UnitTest
{
    [TestClass]
    public class UnitTestDataAndHeader
    {
        [TestMethod]
        public void TestDataFile()
        {
            IData file = new File(@"C:\Users\Admin\Desktop\StegoSystem\Files\test.txt");
            byte[] b = file.GetBytes();
            byte[] expected = new byte[] { 0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64 };
            CollectionAssert.AreEqual(expected, b, "Файл должен содержать \"Hello world\" в ASCII");

            int cm = file.GetHeader().GetOnlyConstSize() + TypeHashSum.NONE.SizeInBytes;

            Assert.AreEqual(8, cm);
        }

        [TestMethod]
        public void TestDataText()
        {
            String expected = "проверка работы кодировки текста";
            IData text = new Text(expected);

            text.GetHeader().SetProperty(HeaderProperty.ENCODE, TypeTextEncoding.UTF_8);

            byte[] tbytes = text.GetBytes();
            String s = Encoding.UTF8.GetString(tbytes);
            Assert.AreEqual(expected, s);

            text.GetHeader().SetProperty(HeaderProperty.ENCODE, TypeTextEncoding.WINDOWS_1251);
            tbytes = text.GetBytes();
            s = Encoding.GetEncoding("windows-1251").GetString(tbytes);
            Assert.AreEqual(expected, s);

            text.GetHeader().SetProperty(HeaderProperty.ENCODE, TypeTextEncoding.CP866);
            tbytes = text.GetBytes();
            s = Encoding.GetEncoding("cp866").GetString(tbytes);
            Assert.AreEqual(expected, s);

            Assert.AreEqual(9, text.GetHeader().GetOnlyConstSize());

        }

        [TestMethod]
        public void TestDataTextASCIIExceptionForCyrillic()
        {
            try {
                String expected = "проверка работы кодировки текста";
                IData text = new Text(expected);
                text.GetHeader().SetProperty(HeaderProperty.ENCODE, TypeTextEncoding.ASCII);
                byte[] tbytes = text.GetBytes();
                String s = Encoding.GetEncoding("ASCII").GetString(tbytes);
                Assert.AreEqual(expected, s);
            } catch(Exception)
            {
                return;
            }
            Assert.Fail("Должно вылететь исключение, так как нельзя преобразовать кириллицу в ASCII");
        }

        [TestMethod]
        public void TestHeader()
        {
            IHeader h = new FileHeader();
            h.SetProperty(HeaderProperty.TYPE_FILE, TypeFile.DOCX);
            h.SetProperty(HeaderProperty.SIZE_FILE, 512u);
            h.SetProperty(HeaderProperty.COMPRESSION_MODE, CompressMode.GZIP);
            h.SetProperty(HeaderProperty.TYPE_HASH_SUM, TypeHashSum.MD5);
            h.SetProperty(HeaderProperty.VALUE_HASH_SUM, new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });

            byte[] bytes = h.GetBytes();

            IHeader h2 = HeaderFactory.CreateFromBytes(bytes);

            CollectionAssert.AreEqual(bytes, h2.GetBytes());

            Assert.AreEqual(h.GetProperty(HeaderProperty.TYPE_FILE), h2.GetProperty(HeaderProperty.TYPE_FILE));

            // При инициализации поле OFFSET_DATA получает значение сумму всех константных полей, 
            // актуальное значение вычисляется только когда вызывается метод GetBytes()
            //Assert.AreEqual(h.GetProperty(HeaderProperty.OFFSET_DATA), h2.GetProperty(HeaderProperty.OFFSET_DATA));

            Assert.AreEqual(h.GetProperty(HeaderProperty.COMPRESSION_MODE), h2.GetProperty(HeaderProperty.COMPRESSION_MODE));
            Assert.AreEqual(h.GetProperty(HeaderProperty.SIZE_FILE), h2.GetProperty(HeaderProperty.SIZE_FILE));
            Assert.AreEqual(h.GetProperty(HeaderProperty.TYPE_HASH_SUM), h2.GetProperty(HeaderProperty.TYPE_HASH_SUM));
            CollectionAssert.AreEqual((byte[])h.GetProperty(HeaderProperty.VALUE_HASH_SUM), (byte[])h2.GetProperty(HeaderProperty.VALUE_HASH_SUM));

        }
    }
}
