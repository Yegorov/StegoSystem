﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.Data
{
    public interface IHeader
    {
        byte[] GetBytes();
        int GetOnlyConstSize();
        void SetProperty(HeaderProperty property, Object value);
        Object GetProperty(HeaderProperty property);
    }

    public enum HeaderProperty
    {
        TYPE_FILE, // Тип файла (1 - txt, 2 - docx, .....) для уменьшения размера только байт
        OFFSET_DATA, // Смещение начала данных (размер заголовка в байтах)
        SIZE_FILE, // Размер файла 4 байта 

        TYPE_HASH_SUM, // Тип хеш суммы (для определения размера) (ожидается строка "md5", "sha-256") (размер хеша определяется по типу)
        VALUE_HASH_SUM, // Значение хеш суммы (массив байт)

        COMPRESSION_MODE,
        ENCODE // Кодировка текста (если текстовый файл 1-ASCII, 2 - utf-8, ...)
    }

    public static class HeaderFactory
    {
        public static IHeader CreateFromBytes(byte[] bytes)
        {
            if(bytes[0] == TypeFile.NONE.Value)
            {
                return TextHeader.CreateTextHeaderFromBytes(bytes);
            }
            else
            {
                return FileHeader.CreateFileHeaderFromBytes(bytes);
            }
        }
    }

    // http://stackoverflow.com/questions/424366/c-sharp-string-enums
    public sealed class TypeFile
    {
        private readonly String name;
        private readonly int value;

        private static readonly int MAX_COUNT_TYPES = 256;
        private static TypeFile[] arrayTypes = new TypeFile[MAX_COUNT_TYPES];
        private static IDictionary<String, TypeFile> dictTypes = new SortedDictionary<string, TypeFile>();

        public static readonly TypeFile NONE = new TypeFile(0, "");
        public static readonly TypeFile UNKNOWN = new TypeFile(1, "unknown");
        public static readonly TypeFile TXT = new TypeFile(2, "txt");
        public static readonly TypeFile DOCX = new TypeFile(3, "docx");
        public static readonly TypeFile ZIP = new TypeFile(4, "zip");
        // Другие типы (всего до 256)

        private TypeFile(int value, String name)
        {
            if (value < 0 && value > MAX_COUNT_TYPES)
                throw new ArgumentOutOfRangeException();

            this.name = name;
            this.value = value;

            TypeFile.arrayTypes[value] = this;
            TypeFile.dictTypes.Add(name, this);
        }

        public static TypeFile GetFileType(int i)
        {
            return TypeFile.arrayTypes[i];
        }
        public static TypeFile GetFileType(String i)
        {
            TypeFile tf = null;
            try
            {
                tf = TypeFile.dictTypes[i];
            }
            catch(Exception)
            {
                return TypeFile.UNKNOWN;
            }

            return tf == null ? TypeFile.UNKNOWN : tf;
        }
        public int Value
        {
            get
            {
                return value;
            }
        }
        public String Name
        {
            get
            {
                return name;
            }
        }

        public override String ToString()
        {
            return name;
        }

    }

    public sealed class CompressMode
    {
        private readonly String name;
        private readonly int value;

        private static readonly int MAX_COUNT_MODES = 256;
        private static CompressMode[] arrayModes = new CompressMode[MAX_COUNT_MODES];
        private static IDictionary<String, CompressMode> dictModes = new SortedDictionary<string, CompressMode>();

        public static readonly CompressMode NONE = new CompressMode(0, "");
        public static readonly CompressMode GZIP = new CompressMode(1, "gzip");
        public static readonly CompressMode DEFLATE = new CompressMode(2, "deflate");
        // Другие типы сжатия (всего до 256)
        private CompressMode(int value, String name)
        {
            if (value < 0 && value > MAX_COUNT_MODES)
                throw new ArgumentOutOfRangeException();

            this.name = name;
            this.value = value;

            CompressMode.arrayModes[value] = this;
            CompressMode.dictModes.Add(name, this);
        }
        public static CompressMode GetCompressMode(String i)
        {
            CompressMode cm = null;
            try
            {
                cm = CompressMode.dictModes[i];
            }
            catch (Exception)
            {
                return CompressMode.NONE;
            }

            return cm == null ? CompressMode.NONE : cm;
        }
        public static CompressMode GetCompressMode(int i)
        {
            return CompressMode.arrayModes[i];
        }

        public int Value
        {
            get
            {
                return value;
            }
        }
        public String Name
        {
            get
            {
                return name;
            }
        }
        public override String ToString()
        {
            return name;
        }

    }

    public sealed class TypeHashSum
    {
        private readonly String name;
        private readonly int value;
        private readonly int sizeHashSumInBits; // размер в битах
        private readonly int sizeHashSumInBytes; // размер в байтах

        private static readonly int MAX_COUNT_TYPES = 256;
        private static TypeHashSum[] arrayTypes = new TypeHashSum[MAX_COUNT_TYPES];
        private static IDictionary<String, TypeHashSum> dictTypes = new SortedDictionary<string, TypeHashSum>();

        public static readonly TypeHashSum NONE = new TypeHashSum(0, "", 0);
        public static readonly TypeHashSum MD5 = new TypeHashSum(1, "md5", 128);
        public static readonly TypeHashSum SHA_1 = new TypeHashSum(2, "sha-1", 160);
        public static readonly TypeHashSum SHA_256 = new TypeHashSum(3, "sha-256", 256);
        // Другие хеш суммы (всего до 256)
        private TypeHashSum(int value, String name, int sizeHashSum)
        {
            if (value < 0 && value > MAX_COUNT_TYPES)
                throw new ArgumentOutOfRangeException();

            this.name = name;
            this.value = value;
            this.sizeHashSumInBits = sizeHashSum;
            this.sizeHashSumInBytes = sizeHashSum / 8;

            TypeHashSum.arrayTypes[value] = this;
            TypeHashSum.dictTypes.Add(name, this);
        }

        public static TypeHashSum GetTypeHashSum(String i)
        {
            TypeHashSum ths = null;
            try
            {
                ths = TypeHashSum.dictTypes[i];
            }
            catch (Exception)
            {
                return TypeHashSum.NONE;
            }

            return ths == null ? TypeHashSum.NONE : ths;
        }

        public static TypeHashSum GetTypeHashSum(int i)
        {
            return TypeHashSum.arrayTypes[i];
        }

        public int Value
        {
            get
            {
                return value;
            }
        }

        public int SizeInBytes
        {
            get
            {
                return sizeHashSumInBytes;
            }
        }

        public int SizeInBits
        {
            get
            {
                return sizeHashSumInBits;
            }
        }

        public String Name
        {
            get
            {
                return name;
            }
        }
        public override String ToString()
        {
            return name;
        }

    }

    public sealed class TypeTextEncoding
    {
        private readonly String name;
        private readonly int value;

        private static readonly int MAX_COUNT_TYPES = 256;
        private static TypeTextEncoding[] arrayTypes = new TypeTextEncoding[MAX_COUNT_TYPES];
        private static IDictionary<String, TypeTextEncoding> dictTypes = new SortedDictionary<string, TypeTextEncoding>();

        public static readonly TypeTextEncoding UNKNOWN = new TypeTextEncoding(0, "unknown");
        public static readonly TypeTextEncoding ASCII = new TypeTextEncoding(1, "ascii");
        public static readonly TypeTextEncoding UTF_8 = new TypeTextEncoding(2, "utf-8");
        public static readonly TypeTextEncoding WINDOWS_1251 = new TypeTextEncoding(3, "windows-1251");
        public static readonly TypeTextEncoding CP866 = new TypeTextEncoding(4, "cp866");
        // Другие кодировки (всего до 256)

        private TypeTextEncoding(int value, String name)
        {
            if (value < 0 && value > MAX_COUNT_TYPES)
                throw new ArgumentOutOfRangeException();

            this.name = name;
            this.value = value;

            TypeTextEncoding.arrayTypes[value] = this;
            TypeTextEncoding.dictTypes.Add(name, this);
        }
        public static TypeTextEncoding GetTypeTextEncoding(int i)
        {
            TypeTextEncoding textEncoding = TypeTextEncoding.arrayTypes[i];
            return textEncoding == null ? TypeTextEncoding.UNKNOWN : textEncoding;
        }
        public static TypeTextEncoding GetTypeTextEncoding(String i)
        {
            TypeTextEncoding tte = null;
            try
            {
                tte = TypeTextEncoding.dictTypes[i];
            }
            catch (Exception)
            {
                return TypeTextEncoding.UNKNOWN;
            }

            return tte == null ? TypeTextEncoding.UNKNOWN : tte;
        }
        public int Value
        {
            get
            {
                return value;
            }
        }

        public String Name
        {
            get
            {
                return name;
            }
        }
        public override String ToString()
        {
            return name;
        }
    }
}
