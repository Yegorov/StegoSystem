﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StegoSystem;

namespace UnitTest
{
    [TestClass]
    public class UnitTestStegoKey
    {
        /*
        Некотарая информация по поводу написания тестов
        https://msdn.microsoft.com/ru-ru/library/ms182532.aspx
        */

        [TestMethod]
        public void TestForCreateStegoKeyAndFromBase64()
        {
            StegoKey sk = new StegoKey();
            sk.Offset = 12345;
            sk.EmbededMode = (byte)EmbededModeValue.ONE_BYTE_POINTER_TWO_LEAST_BIT;
            sk.TypeEncryption = (byte)TypeEncryptionValue.AES_128;
            sk.SetIV(new byte[] { 1, 2, 3, 4, 5, 7, 8, 9, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 3, 2, 1 });
            sk.SetKey(new byte[] { 5, 4, 3, 2, 1, 5, 6, 7, 8, 5, 4, 45, 6, 7, 7, 6, 4, 5, 45, 5, 6, 5, 4, 3, 2, 1, 2, 3, 4, 5, 6, 7, 8, 7, 5, 4, 3, 5, 4 });

            byte[] bs = sk.ToBytes();
            String s = sk.ToBase64String();

            StegoKey sk2 = StegoKey.CreateFromBase64String(s);

            Assert.IsTrue(sk.Equals(sk2), "Метод Equals в классе StegoKey работает не корректно");
            Assert.AreEqual<StegoKey>(sk, sk2);
        }
        [TestMethod]
        public void TestForExceptionCreateIV1()
        {
            try
            {
                StegoKey s1 = new StegoKey(1, 1, 1, new byte[999999], new byte[] { 1 });
            }
            catch (ArgumentOutOfRangeException ex)
            {
                StringAssert.Contains(ex.ParamName, "Размер вектора инициализиции не должен превышать");
                return;
            }
            Assert.Fail("Не было брошено ArgumentOutOfRangeException исключения");
        }

        [TestMethod]
        public void TestForExceptionCreateIV2()
        {
            try
            {
                StegoKey s1 = new StegoKey(1, 1, 1, null, new byte[] { 1 });
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.ParamName, "iv не должен быть равным null");
                return;
            }
            Assert.Fail("Не было брошено ArgumentNullException исключения");
        }

        [TestMethod]
        public void TestForExceptionCreateKey1()
        {
            try
            {
                StegoKey s1 = new StegoKey(1, 1, 1, new byte[1], new byte[9999999]);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                StringAssert.Contains(ex.ParamName, "Размер ключа шифрования не должен превышать");
                return;
            }
            Assert.Fail("Не было брошено ArgumentOutOfRangeException исключения");
        }

        [TestMethod]
        public void TestForExceptionCreateKey2()
        {
            try
            {
                StegoKey s1 = new StegoKey(1, 1, 1, new byte[] { 1 }, null);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.ParamName, "key не должен быть равным null");
                return;
            }
            Assert.Fail("Не было брошено ArgumentNullException исключения");
        }
    }
}
