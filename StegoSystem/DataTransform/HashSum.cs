﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.DataTransform
{
    public class NoneHashSum : IHashSum
    {
        private readonly String hashName = "";

        public byte[] GetHashBytes(Stream s)
        {
            return null;
        }

        public byte[] GetHashBytes(byte[] bytes)
        {
            return null;
        }

        public string GetHashName()
        {
            return hashName;
        }
    }

    public class MD5HashSum : IHashSum
    {
        private readonly String hashName = "md5";
        
        public byte[] GetHashBytes(Stream s)
        {
            MD5 md5 = MD5.Create();
            md5.ComputeHash(s);
            return md5.Hash;
        }

        public byte[] GetHashBytes(byte[] bytes)
        {
            MD5 md5 = MD5.Create();
            md5.ComputeHash(bytes);
            return md5.Hash;
        }

        public string GetHashName()
        {
            return hashName;
        }
    }

    public class SHA_1HashSum : IHashSum
    {
        private readonly String hashName = "sha-1";

        public byte[] GetHashBytes(Stream s)
        {
            SHA1 sha1 = SHA1.Create();
            sha1.ComputeHash(s);
            return sha1.Hash;
        }

        public byte[] GetHashBytes(byte[] bytes)
        {
            SHA1 sha1 = SHA1.Create();
            sha1.ComputeHash(bytes);
            return sha1.Hash;
        }

        public string GetHashName()
        {
            return hashName;
        }
    }

    public class SHA_256HashSum : IHashSum
    {
        private readonly String hashName = "sha-256";

        public byte[] GetHashBytes(Stream s)
        {
            SHA256 sha256 = SHA256.Create();
            sha256.ComputeHash(s);
            return sha256.Hash;
        }

        public byte[] GetHashBytes(byte[] bytes)
        {
            SHA256 sha256 = SHA256.Create();
            sha256.ComputeHash(bytes);
            return sha256.Hash;
        }

        public string GetHashName()
        {
            return hashName;
        }
    }
}
