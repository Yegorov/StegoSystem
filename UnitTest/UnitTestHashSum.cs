﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using StegoSystem.DataTransform;
using System.Linq;
using System.IO;

namespace UnitTest
{
    [TestClass]
    public class UnitTestHashSum
    {
        [TestMethod]
        public void TestCalculateHashSumFromBytes()
        {
            String str = "Hello world";
            byte[] bytesFromString = Encoding.UTF8.GetBytes(str);
            byte[] md5Expected = new byte[] { 0x3e, 0x25, 0x96, 0x0a, 0x79, 0xdb, 0xc6, 0x9b, 0x67, 0x4c, 0xd4, 0xec, 0x67, 0xa7, 0x2c, 0x62 };
            String md5Str = "md5";
            byte[] sha_1Expected = new byte[] { 0x7b, 0x50, 0x2c, 0x3a, 0x1f, 0x48, 0xc8, 0x60, 0x9a, 0xe2, 0x12, 0xcd, 0xfb, 0x63, 0x9d, 0xee, 0x39, 0x67, 0x3f, 0x5e };
            String sha_1Str = "sha-1";
            byte[] sha_256Expected = new byte[] { 0x64, 0xec, 0x88, 0xca, 0x00, 0xb2, 0x68, 0xe5, 0xba, 0x1a, 0x35, 0x67, 0x8a, 0x1b, 0x53, 0x16, 0xd2, 0x12, 0xf4, 0xf3, 0x66, 0xb2, 0x47, 0x72, 0x32, 0x53, 0x4a, 0x8a, 0xec, 0xa3, 0x7f, 0x3c };
            String sha_256Str = "sha-256";

            IHashSum sum = new MD5HashSum();
            byte[] hashBytes = sum.GetHashBytes(bytesFromString);
            CollectionAssert.AreEqual(md5Expected, hashBytes);
            StringAssert.Equals(md5Str, sum.GetHashName());

            sum = new SHA_1HashSum();
            hashBytes = sum.GetHashBytes(bytesFromString);
            CollectionAssert.AreEqual(sha_1Expected, hashBytes);
            StringAssert.Equals(sha_1Str, sum.GetHashName());

            sum = new SHA_256HashSum();
            hashBytes = sum.GetHashBytes(bytesFromString);
            CollectionAssert.AreEqual(sha_256Expected, hashBytes);
            StringAssert.Equals(sha_256Str, sum.GetHashName());
        }
        [TestMethod]
        public void TestCalculateHashSumFromStream()
        {
            String str = "Hello world";
            byte[] bytesFromString = Encoding.UTF8.GetBytes(str);
            byte[] md5Expected = new byte[] { 0x3e, 0x25, 0x96, 0x0a, 0x79, 0xdb, 0xc6, 0x9b, 0x67, 0x4c, 0xd4, 0xec, 0x67, 0xa7, 0x2c, 0x62 };
            byte[] sha_1Expected = new byte[] { 0x7b, 0x50, 0x2c, 0x3a, 0x1f, 0x48, 0xc8, 0x60, 0x9a, 0xe2, 0x12, 0xcd, 0xfb, 0x63, 0x9d, 0xee, 0x39, 0x67, 0x3f, 0x5e };
            byte[] sha_256Expected = new byte[] { 0x64, 0xec, 0x88, 0xca, 0x00, 0xb2, 0x68, 0xe5, 0xba, 0x1a, 0x35, 0x67, 0x8a, 0x1b, 0x53, 0x16, 0xd2, 0x12, 0xf4, 0xf3, 0x66, 0xb2, 0x47, 0x72, 0x32, 0x53, 0x4a, 0x8a, 0xec, 0xa3, 0x7f, 0x3c };

            Stream s = new MemoryStream(bytesFromString);

            IHashSum sum = new MD5HashSum();
            byte[] hashBytes = sum.GetHashBytes(s);
            CollectionAssert.AreEqual(md5Expected, hashBytes);

            s.Seek(0, SeekOrigin.Begin);
            sum = new SHA_1HashSum();
            hashBytes = sum.GetHashBytes(s);
            CollectionAssert.AreEqual(sha_1Expected, hashBytes);

            s.Seek(0, SeekOrigin.Begin);
            sum = new SHA_256HashSum();
            hashBytes = sum.GetHashBytes(s);
            CollectionAssert.AreEqual(sha_256Expected, hashBytes);
        }
    }
}
