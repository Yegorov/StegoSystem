﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StegoSystem.Data;
using StegoSystem.DataTransform;
using StegoSystem.StegoTransform;
using System.Drawing;

namespace StegoSystem
{
    public class StegoSystem
    {
        private StegoKey key;
        private Bitmap container;
        private Bitmap result;

        // embed
        private int offset;
        private int maxDiffusion;

        private String hashSum;
        private String compression;
        private String encryption;
        private bool tryForceEmbed; // Без грубой проверки, если true то вылетет exception при внедрении

        //extract
        private IHeader header;
        private byte[] bytes;
        // private IDataTransformCoder dataCoder;
        // private IDataTransformEncoder dataEncoder;
        // 
        // private IStegoTransformEmbed stegoEmbed;
        // private IStegoTransformExtract stegoExtract;

        public StegoSystem()
        {
            container = null;
            offset = -1;
            maxDiffusion = -1;

            hashSum = null;
            compression = null;
            encryption = null;
            tryForceEmbed = false;
        }
        public StegoSystem SetContainer(Bitmap container)
        {
            this.container = container;
            return this;
        }

        public StegoSystem SetOffset(int offset)
        {
            this.offset = offset;
            return this;
        }

        public StegoSystem SetMaxDiffusion(int maxDiffusion)
        {
            this.maxDiffusion = maxDiffusion;
            return this;
        }
        public StegoSystem SetHashSum(String hashSum)
        {
            this.hashSum = hashSum;
            return this;
        }
        public StegoSystem SetCompression(String compression)
        {
            this.compression = compression;
            return this;
        }
        public StegoSystem SetEncryption(String encryption)
        {
            this.encryption = encryption;
            return this;
        }
        public StegoSystem SetStegoKeyFromFile(String path)
        {
            key = StegoKey.CreateFromBase64String(System.IO.File.ReadAllText(path));
            return this;
        }
        public StegoSystem SetTryForceEmbed(bool tryForceEmbed)
        {
            this.tryForceEmbed = tryForceEmbed;
            return this;
        }
        public StegoKey GetStegoKey()
        {
            return key;
        }


        public void HideString(String text, String encoding)
        {
            if (offset == -1)
                throw new Exception("Error offset");
            if(maxDiffusion == -1)
                throw new Exception("Error maxDiffusion");
            if(container == null)
                throw new Exception("Error container");

            if (hashSum == null)
                throw new Exception("Error hashSum");
            if (compression == null)
                throw new Exception("Error compression");
            if (encryption == null)
                throw new Exception("Error encryption");

            IData data = new Text(text);
            data.GetHeader().SetProperty(HeaderProperty.ENCODE, TypeTextEncoding.GetTypeTextEncoding(encoding));
            HideData(data);
        }

        private void HideData(IData data)
        {
            IDataTransformCoder coder = new DataCoder(
                HashSumFactory.CreateFromString(hashSum),
                CompressionFactory.CreateFromString(compression),
                SymmetricEncryptionFactory.CreateFromString(encryption));

            byte[] dataTransformBytes = coder.TransfromToBytes(data);
            key = coder.GetStegoKey();

            int countPixelsInContainer = container.Height * container.Width;

            int sizeEmbedData = dataTransformBytes.Length;

            if (!tryForceEmbed)
            {
                int calculateSize = StegoCalculateForEmbed.CalculateMaxPixelsForEmbedFromString(EmbededModeValueConst.ONE_BYTE_POINTER_TWO_LEAST_BIT, offset, maxDiffusion, sizeEmbedData);
                if (calculateSize > countPixelsInContainer)
                    throw new Exception("Не возможно внедрить данные в контейнер (мало места, расчет по максимальному рассеиванию)");
            }

            IStegoTransformEmbed embed = StegoTransformEmbedFactory.CreateFromString(EmbededModeValueConst.ONE_BYTE_POINTER_TWO_LEAST_BIT);

            result = embed.EmbedFromBytes(offset, maxDiffusion, dataTransformBytes, container, key);
        }

        public void HideFile(String path)
        {
            if (offset == -1)
                throw new Exception("Error offset");
            if (maxDiffusion == -1)
                throw new Exception("Error maxDiffusion");
            if (container == null)
                throw new Exception("Error container");

            if (hashSum == null)
                throw new Exception("Error hashSum");
            if (compression == null)
                throw new Exception("Error compression");
            if (encryption == null)
                throw new Exception("Error encryption");

            IData data = new File(path);
            HideData(data);
        }

        public void ExtractBytesAndHeader()
        {
            if (container == null)
                throw new Exception("Error container");
            if (key == null)
                throw new Exception("Error key");

            IStegoTransformExtract extract = StegoTransformExtractFactory.CreateFromNumber(key.EmbededMode);

            byte[] stegoBytes = extract.ExtractToBytes(container, key);

            IDataTransformEncoder encoder = new DataEncoder(key);
            bytes = encoder.TransfromFromBytes(stegoBytes);
            header = encoder.GetHeader();
        }
        public byte[] GetBytes()
        {
            return bytes;
        }

        public IHeader GetHeader()
        {
            return header;
        }

        public void SaveStegoKey(String path)
        {
            System.IO.File.WriteAllText(path, key.ToBase64String());
        }
        public void SaveResultBitmap(String path)
        {
            result.Save(path);
        }

        public void Dispose()
        {
            if(container != null)
                container.Dispose();

            if (result != null)
                result.Dispose();
        }
    }
}
