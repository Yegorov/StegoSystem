﻿namespace StegoGUI
{
    partial class FormExtract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSetImg = new System.Windows.Forms.Button();
            this.buttonSetKey = new System.Windows.Forms.Button();
            this.buttonExtract = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.openFileDialogImage = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogKey = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialogSaveFile = new System.Windows.Forms.SaveFileDialog();
            this.labelProgress = new System.Windows.Forms.Label();
            this.pictureBoxProgress = new System.Windows.Forms.PictureBox();
            this.textBoxText = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProgress)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSetImg
            // 
            this.buttonSetImg.Location = new System.Drawing.Point(22, 22);
            this.buttonSetImg.Name = "buttonSetImg";
            this.buttonSetImg.Size = new System.Drawing.Size(101, 23);
            this.buttonSetImg.TabIndex = 0;
            this.buttonSetImg.Text = "Set image";
            this.buttonSetImg.UseVisualStyleBackColor = true;
            this.buttonSetImg.Click += new System.EventHandler(this.buttonSetImg_Click);
            // 
            // buttonSetKey
            // 
            this.buttonSetKey.Location = new System.Drawing.Point(141, 22);
            this.buttonSetKey.Name = "buttonSetKey";
            this.buttonSetKey.Size = new System.Drawing.Size(101, 23);
            this.buttonSetKey.TabIndex = 1;
            this.buttonSetKey.Text = "Set key";
            this.buttonSetKey.UseVisualStyleBackColor = true;
            this.buttonSetKey.Click += new System.EventHandler(this.buttonSetKey_Click);
            // 
            // buttonExtract
            // 
            this.buttonExtract.Location = new System.Drawing.Point(22, 62);
            this.buttonExtract.Name = "buttonExtract";
            this.buttonExtract.Size = new System.Drawing.Size(431, 23);
            this.buttonExtract.TabIndex = 2;
            this.buttonExtract.Text = "Extract";
            this.buttonExtract.UseVisualStyleBackColor = true;
            this.buttonExtract.Click += new System.EventHandler(this.buttonExtract_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Header:";
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(12, 199);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 4;
            this.buttonSave.Text = "Save file";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // labelProgress
            // 
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(300, 46);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(126, 13);
            this.labelProgress.TabIndex = 30;
            this.labelProgress.Text = "Please, wait for extract ...";
            // 
            // pictureBoxProgress
            // 
            this.pictureBoxProgress.Image = global::StegoGUI.Properties.Resources.Win8ProgressIndicator;
            this.pictureBoxProgress.Location = new System.Drawing.Point(264, 22);
            this.pictureBoxProgress.Name = "pictureBoxProgress";
            this.pictureBoxProgress.Size = new System.Drawing.Size(189, 23);
            this.pictureBoxProgress.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxProgress.TabIndex = 29;
            this.pictureBoxProgress.TabStop = false;
            // 
            // textBoxText
            // 
            this.textBoxText.Location = new System.Drawing.Point(141, 199);
            this.textBoxText.Multiline = true;
            this.textBoxText.Name = "textBoxText";
            this.textBoxText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxText.Size = new System.Drawing.Size(312, 66);
            this.textBoxText.TabIndex = 31;
            // 
            // FormExtract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 277);
            this.Controls.Add(this.textBoxText);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.pictureBoxProgress);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonExtract);
            this.Controls.Add(this.buttonSetKey);
            this.Controls.Add(this.buttonSetImg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormExtract";
            this.Text = "AStego | Extract";
            this.VisibleChanged += new System.EventHandler(this.FormExtract_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProgress)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSetImg;
        private System.Windows.Forms.Button buttonSetKey;
        private System.Windows.Forms.Button buttonExtract;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.OpenFileDialog openFileDialogImage;
        private System.Windows.Forms.OpenFileDialog openFileDialogKey;
        private System.Windows.Forms.SaveFileDialog saveFileDialogSaveFile;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.PictureBox pictureBoxProgress;
        private System.Windows.Forms.TextBox textBoxText;
    }
}