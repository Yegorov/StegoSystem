﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.Data
{
    public interface IData
    {
        byte[] GetBytes();
        int GetSize();
        Stream GetStream();
        IHeader GetHeader();
    }
}
