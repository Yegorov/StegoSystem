﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.DataTransform
{
    public class NoneEncryption : ISymmetricEncryption
    {
        private readonly String encryptionName = TypeEncryptionValueConst.NONE;

        public byte[] GetBytes(byte[] bytes, DataMode mode)
        {
            return bytes;
        }

        public Stream GetStream(Stream dataStream, Stream outStream, DataMode mode)
        {
            dataStream.CopyTo(outStream);
            return outStream;
        }
        public void SetKeyIV(byte[] key, byte[] iv)
        {

        }
        public byte[] GetKey()
        {
            return new byte[] { };
        }
        public  byte[] GetIV()
        {
            return new byte[] { };
        }

        public string GetSymmetricEncryptionName()
        {
            return encryptionName;
        }
        public TypeEncryptionValue GetTypeEncryptionValue()
        {
            return TypeEncryptionValue.NONE;
        }
    }

    public class AES128Encryption : ISymmetricEncryption
    {
        private readonly String encryptionName = TypeEncryptionValueConst.AES_128;
        private AesCryptoServiceProvider AESProvider;
        private byte[] key;
        private byte[] iv;

        public byte[] GetBytes(byte[] bytes, DataMode mode)
        {
            if (AESProvider != null)
            {
                AESProvider.Clear();
            }

            AESProvider = new AesCryptoServiceProvider();
            AESProvider.KeySize = 128;
            GenerateAESKey();

            ICryptoTransform cryptoTransform;
            if (mode == DataMode.IN)
            {
                cryptoTransform = AESProvider.CreateEncryptor(AESProvider.Key, AESProvider.IV);
                key = AESProvider.Key;
                iv = AESProvider.IV;
            }
            else
                cryptoTransform = AESProvider.CreateDecryptor(this.key, this.iv);

            // Write the decrypted value to the encryption stream
            MemoryStream memoryStreamOutput = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStreamOutput, cryptoTransform, CryptoStreamMode.Write);
            cryptoStream.Write(bytes, 0, bytes.Length);
            cryptoStream.FlushFinalBlock();

            // Read encrypted value back out of the stream
            memoryStreamOutput.Position = 0;
            byte[] encryptedBytes = new byte[memoryStreamOutput.Length];
            memoryStreamOutput.Read(encryptedBytes, 0, encryptedBytes.Length);

            cryptoStream.Close();
            memoryStreamOutput.Close();

            return encryptedBytes;
        }
        public Stream GetStream(Stream dataStream, Stream outStream, DataMode mode)
        {
            if (AESProvider != null)
            {
                AESProvider.Clear();
            }

            AESProvider = new AesCryptoServiceProvider();
            AESProvider.KeySize = 128;

            GenerateAESKey();

            ICryptoTransform cryptoTransform;
            if (mode == DataMode.IN)
            {
                cryptoTransform = AESProvider.CreateEncryptor(AESProvider.Key, AESProvider.IV);
                key = AESProvider.Key;
                iv = AESProvider.IV;
            }
            else
                cryptoTransform = AESProvider.CreateDecryptor(this.key, this.iv);

            CryptoStream cryptoStream = new CryptoStream(outStream, cryptoTransform, CryptoStreamMode.Write);
            dataStream.CopyTo(cryptoStream);
            cryptoStream.FlushFinalBlock();

            return cryptoStream;
        }
        private void GenerateAESKey()
        {
            AESProvider.GenerateIV();
            AESProvider.GenerateKey();
        }

        public void SetKeyIV(byte[] key, byte[] iv)
        {
            this.key = key;
            this.iv = iv;
        }
        public byte[] GetKey()
        {
            return key;
        }
        public byte[] GetIV()
        {
            return iv;
        }

        public string GetSymmetricEncryptionName()
        {
            return encryptionName;
        }
        public TypeEncryptionValue GetTypeEncryptionValue()
        {
            return TypeEncryptionValue.AES_128;
        }
    }

    public class TripleDES128Encryption : ISymmetricEncryption
    {
        private readonly String encryptionName = TypeEncryptionValueConst.TRIPLE_DES_128;
        private TripleDESCryptoServiceProvider tripleDESProvider;
        private byte[] key;
        private byte[] iv;

        public byte[] GetBytes(byte[] bytes, DataMode mode)
        {
            if (tripleDESProvider != null)
            {
                tripleDESProvider.Clear();
            }

            tripleDESProvider = new TripleDESCryptoServiceProvider();
            tripleDESProvider.KeySize = 128;
            Generate3DESKey();

            ICryptoTransform cryptoTransform;
            if (mode == DataMode.IN)
            {
                cryptoTransform = tripleDESProvider.CreateEncryptor(tripleDESProvider.Key, tripleDESProvider.IV);
                key = tripleDESProvider.Key;
                iv = tripleDESProvider.IV;
            }
            else
                cryptoTransform = tripleDESProvider.CreateDecryptor(this.key, this.iv);

            // Write the decrypted value to the encryption stream
            MemoryStream memoryStreamOutput = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStreamOutput, cryptoTransform, CryptoStreamMode.Write);
            cryptoStream.Write(bytes, 0, bytes.Length);
            cryptoStream.FlushFinalBlock();

            // Read encrypted value back out of the stream
            memoryStreamOutput.Position = 0;
            byte[] encryptedBytes = new byte[memoryStreamOutput.Length];
            memoryStreamOutput.Read(encryptedBytes, 0, encryptedBytes.Length);

            cryptoStream.Close();
            memoryStreamOutput.Close();

            return encryptedBytes;
        }
        public Stream GetStream(Stream dataStream, Stream outStream, DataMode mode)
        {
            if (tripleDESProvider != null)
            {
                tripleDESProvider.Clear();
            }

            tripleDESProvider = new TripleDESCryptoServiceProvider();
            tripleDESProvider.KeySize = 128;
            Generate3DESKey();

            ICryptoTransform cryptoTransform;
            if (mode == DataMode.IN)
            {
                cryptoTransform = tripleDESProvider.CreateEncryptor(tripleDESProvider.Key, tripleDESProvider.IV);
                key = tripleDESProvider.Key;
                iv = tripleDESProvider.IV;
            }
            else
                cryptoTransform = tripleDESProvider.CreateDecryptor(this.key, this.iv);

            CryptoStream cryptoStream = new CryptoStream(outStream, cryptoTransform, CryptoStreamMode.Write);
            dataStream.CopyTo(cryptoStream);
            cryptoStream.FlushFinalBlock();

            return cryptoStream;
        }
        private void Generate3DESKey()
        {
            tripleDESProvider.GenerateIV();
            tripleDESProvider.GenerateKey();
        }

        public void SetKeyIV(byte[] key, byte[] iv)
        {
            this.key = key;
            this.iv = iv;
        }
        public byte[] GetKey()
        {
            return key;
        }
        public byte[] GetIV()
        {
            return iv;
        }

        public string GetSymmetricEncryptionName()
        {
            return encryptionName;
        }
        public TypeEncryptionValue GetTypeEncryptionValue()
        {
            return TypeEncryptionValue.TRIPLE_DES_128;
        }
    }
}
