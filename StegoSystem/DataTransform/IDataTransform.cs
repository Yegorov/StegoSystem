﻿using StegoSystem.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.DataTransform
{
    public interface IDataTransformCoder
    {
        byte[] TransfromToBytes(IData data);
        Stream TransformToStream(IData data);
        StegoKey GetStegoKey();
    }

    public interface IDataTransformEncoder
    {
        byte[] TransfromFromBytes(byte[] bytes);
        Stream TransformFromStream(Stream stream);
        IHeader GetHeader();
        void SetStegoKey(StegoKey stegoKey); // StegoKey должен быть в конструкторе 
    }

    public interface IHashSum
    {
        byte[] GetHashBytes(byte[] bytes);
        byte[] GetHashBytes(Stream s);
        String GetHashName();
    }

    public interface ICompression
    {
        Stream GetStream(Stream dataStream, Stream outStream, DataMode mode);
        byte[] GetBytes(byte[] bytes, DataMode mode);
        String GetCompressName();
    }

    public interface ISymmetricEncryption
    {
        Stream GetStream(Stream dataStream, Stream outStream, DataMode mode);
        byte[] GetBytes(byte[] bytes, DataMode mode);
        String GetSymmetricEncryptionName();
        TypeEncryptionValue GetTypeEncryptionValue();
        void SetKeyIV(byte[] key, byte[] iv);
        byte[] GetKey();
        byte[] GetIV();
    }

    public enum DataMode
    {
        IN, // Шифрование/Сжатие
        OUT // Дешифрование/Разжатие
    }
}
