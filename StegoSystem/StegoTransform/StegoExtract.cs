﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.StegoTransform
{
    // ПРОВЕРИТЬ ЗАКРЫТИЕ ВСЕХ ПОТОКОВ!!!! tre finaly close or using
    // Написать фабрики для StegoTransform. Фабрики собрать в одном месте
    // Переписать интерфейс IDataTransform DataCoder/Encoder чтобы стего ключ был входным параметром
    // заменить числа 4 на BYTES_IN_PIXEL
    // Тесты
    public class StegoExtract : IStegoTransformExtract
    {
        private static readonly EmbededModeValue embededModeValue = EmbededModeValue.ONE_BYTE_POINTER_TWO_LEAST_BIT;
        private static readonly int BYTES_IN_PIXEL = 4;
        public byte[] ExtractToBytes(Bitmap bitmap, StegoKey key)
        {
            //Bitmap resultImage = new Bitmap(bitmap.Width, bitmap.Height);

            // Переводим Bitmap в поток 
            int sizeColorBytesInContainer = bitmap.Width * bitmap.Height * 4;
            Stream srcStream = new MemoryStream(sizeColorBytesInContainer);
            BinaryWriter srcStreamWriter = new BinaryWriter(srcStream);
            BinaryReader srcStreamReader = new BinaryReader(srcStream);

            BitmapToStream(srcStreamWriter, bitmap);
            srcStream.Seek(0, SeekOrigin.Begin);
            byte[] bytes = ExtractedStream((int)key.Offset, srcStreamReader);
            srcStream.Close();
            return bytes;
            //EmbedStream(offset, maxDiffusion, bytes, srcStreamWriter, srcStreamReader);
            //srcStream.Seek(0, SeekOrigin.Begin);
            //StreamToBitmap(srcStreamReader, resultImage);

        }
        private byte[] ExtractedStream(int offset, BinaryReader srcStreamReader)
        {
            MemoryStream dstStream = new MemoryStream();
            BinaryWriter dstStreamWriter = new BinaryWriter(dstStream);


            srcStreamReader.BaseStream.Seek(offset * BYTES_IN_PIXEL, SeekOrigin.Begin);

            Color color1, color2, color3, color4;
            byte pointer = 0;
            byte data1 = 0;
            byte data2 = 0;

            while(srcStreamReader.BaseStream.Position < srcStreamReader.BaseStream.Length - 4 * 4)
            {
                color1 = Color.FromArgb(srcStreamReader.ReadInt32());
                color2 = Color.FromArgb(srcStreamReader.ReadInt32());
                color3 = Color.FromArgb(srcStreamReader.ReadInt32());
                color4 = Color.FromArgb(srcStreamReader.ReadInt32());

                pointer = (byte)(((color1.R & 3) << 6) | ((color1.G & 3) << 4) | ((color1.B & 3) << 2) | ((color2.R & 3) << 0));
                data1   = (byte)(((color2.G & 3) << 6) | ((color2.B & 3) << 4) | ((color3.R & 3) << 2) | ((color3.G & 3) << 0));
                data2   = (byte)(((color3.B & 3) << 6) | ((color4.R & 3) << 4) | ((color4.G & 3) << 2) | ((color4.B & 3) << 0));

                if ((pointer == 255) || (pointer == 254))
                    break;

                dstStreamWriter.Write(data1);
                dstStreamWriter.Write(data2);

                srcStreamReader.BaseStream.Seek(pointer * 4, SeekOrigin.Current); // Перемещаемся в потоке на заданное число пикселей в указателе
            }

            if (pointer == 255) // 1 байт
            {
                dstStreamWriter.Write(data1);
            }
            else if(pointer == 254) // 2 байта
            {
                dstStreamWriter.Write(data1);
                dstStreamWriter.Write(data2);
            }

            return dstStream.ToArray();
        }
        private void BitmapToStream(BinaryWriter streamWriter, Bitmap bitmap)
        {
            Color color;
            for (int y = 0; y < bitmap.Height; ++y)
            {
                for (int x = 0; x < bitmap.Width; ++x)
                {
                    color = bitmap.GetPixel(x, y);
                    streamWriter.Write(color.ToArgb());
                }
            }
        }

        private void StreamToBitmap(BinaryReader streamReader, Bitmap bitmap)
        {
            Color color;
            for (int y = 0; y < bitmap.Height; ++y)
            {
                for (int x = 0; x < bitmap.Width; ++x)
                {
                    color = Color.FromArgb(streamReader.ReadInt32());
                    bitmap.SetPixel(x, y, color);
                }
            }
        }

        public Stream ExtractToStream(Bitmap bitmap, StegoKey key)
        {
            throw new NotImplementedException();
        }
    }

    public static class StegoTransformExtractFactory
    {
        public static IStegoTransformExtract CreateFromNumber(byte n)
        {
            EmbededModeValue val = (EmbededModeValue)n;

            if (val == EmbededModeValue.ONE_BYTE_POINTER_TWO_LEAST_BIT)
                return new StegoExtract();

            throw new Exception("Не найден алгоритм для извлечения данных");
        }
        public static IStegoTransformExtract CreateFromString(String nameStego)
        {
            if (nameStego == EmbededModeValueConst.ONE_BYTE_POINTER_TWO_LEAST_BIT)
                return new StegoExtract();

            throw new Exception("Не найден алгоритм для извлечения данных");
        }
    }
}
