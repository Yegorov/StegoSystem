﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using StegoSystem;

namespace StegoGUI
{
    public partial class FormEmbed : Form
    {
        private StegoSystem.StegoSystem stegoSystem;

        public FormEmbed()
        {
            InitializeComponent();

            comboBoxStego.Items.Add("One byte pointer two least bit");
            comboBoxStego.SelectedIndex = 0;

            comboBoxHashSum.Items.Add("None");
            comboBoxHashSum.Items.Add("md5");
            comboBoxHashSum.Items.Add("sha-1");
            comboBoxHashSum.Items.Add("sha-256");
            comboBoxHashSum.SelectedIndex = 0;

            comboBoxCompression.Items.Add("None");
            comboBoxCompression.Items.Add("deflate");
            comboBoxCompression.Items.Add("gzip");
            comboBoxCompression.SelectedIndex = 0;

            comboBoxEncryption.Items.Add("None");
            comboBoxEncryption.Items.Add("AES-128");
            comboBoxEncryption.Items.Add("3DES-128");
            comboBoxEncryption.SelectedIndex = 0;

            comboBoxEncoding.Items.Add("utf-8");
            comboBoxEncoding.Items.Add("windows-1251");
            comboBoxEncoding.Items.Add("cp866");
            comboBoxEncoding.Items.Add("ascii");
            comboBoxEncoding.SelectedIndex = 0;


            radioButtonFile.Checked = true;
            EnableOrDisableTextFile();

            labelProgress.Visible = false;
            pictureBoxProgress.Visible = false;

            labelTip.Visible = true;

            buttonSaveImage.Enabled = false;
            buttonSaveKey.Enabled = false;
        }

        private void BlockUIAndShowProgress()
        {
            labelTip.Visible = false;
            labelProgress.Visible = true;
            pictureBoxProgress.Visible = true;


            foreach(Control c in this.Controls)
            {
                if (c.Name == "labelProgress" || c.Name == "pictureBoxProgress") continue;
                c.Enabled = false;
            }
        }

        private void UnBlockUIAndHideProgress()
        {

            foreach (Control c in this.Controls)
            {
                c.Enabled = true;
            }
            EnableOrDisableTextFile();
            labelProgress.Visible = false;
            pictureBoxProgress.Visible = false;
            labelTip.Visible = true;
        }

        private void EnableOrDisableTextFile()
        {
            if (radioButtonFile.Checked)
            {
                label1.Enabled = true;
                labelFileName.Enabled = true;
                buttonChooseFile.Enabled = true;

                textBoxText.Enabled = false;
                comboBoxEncoding.Enabled = false;
            }
            else if(radioButtonText.Checked)
            {
                label1.Enabled = false;
                labelFileName.Enabled = false;
                buttonChooseFile.Enabled = false;

                textBoxText.Enabled = true;
                comboBoxEncoding.Enabled = true;
            }

        }

        private void trackBarDiffusion_Scroll(object sender, EventArgs e)
        {
            textBoxDiffusion.Text = String.Format("{0}", trackBarDiffusion.Value);
        }

        private void textBoxOffset_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBoxOffset.Text, "  ^ [0-9]"))
            {
                textBoxOffset.Text = "";
            }
        }

        private void textBoxOffset_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void buttonChooseImage_Click(object sender, EventArgs e)
        {
            openFileDialogImage.CheckFileExists = true;
            openFileDialogImage.Filter = "Images (*.bmp,*.png,*.jpg,*gif)|*.bmp;*.png;*.jpg;*gif";
            if (openFileDialogImage.ShowDialog() == DialogResult.OK)
            {
                labelImageName.Text = openFileDialogImage.SafeFileName;
            }
        }

        private void buttonChooseFile_Click(object sender, EventArgs e)
        {
            openFileDialogFile.CheckFileExists = true;
            if (openFileDialogFile.ShowDialog() == DialogResult.OK)
            {
                FileInfo finfo = new FileInfo(openFileDialogFile.FileName);
                if (finfo.Length > 1 * 1024 * 1024)
                {
                    MessageBox.Show("File size greater than 1 MB", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    labelFileName.Text = "";
                    openFileDialogFile.FileName = "";
                }
                else
                {
                    labelFileName.Text = openFileDialogFile.SafeFileName;
                }
            }
        }

        private void radioButtonText_CheckedChanged(object sender, EventArgs e)
        {
            EnableOrDisableTextFile();
        }

        private void radioButtonFile_CheckedChanged(object sender, EventArgs e)
        {
            EnableOrDisableTextFile();
        }

        private void buttonEmbed_Click(object sender, EventArgs e)
        {
            if (stegoSystem != null)
                stegoSystem.Dispose();

            if (labelImageName.Text.SequenceEqual(""))
            {
                MessageBox.Show("Image is not choosen!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (radioButtonFile.Checked && labelFileName.Text.SequenceEqual(""))
            {
                MessageBox.Show("File is not choosen!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (radioButtonText.Checked && textBoxText.Text.SequenceEqual(""))
            {
                MessageBox.Show("Not input text to embed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            BlockUIAndShowProgress();
            int offset = Int32.Parse(textBoxOffset.Text);
            int maxDiffusion = trackBarDiffusion.Value;
            String imgName = openFileDialogImage.FileName;
            String hashSum = (String)comboBoxHashSum.SelectedItem;
            if (hashSum.SequenceEqual("None"))
                hashSum = "";
            String compression = (String)comboBoxCompression.SelectedItem;
            if (compression.SequenceEqual("None"))
                compression = "";
            String encryption = (String)comboBoxEncryption.SelectedItem;
            if (encryption.SequenceEqual("None"))
                encryption = "";
            bool force = checkBoxForce.Checked;

            String encoding = (String)comboBoxEncoding.SelectedItem;
            String fileName = openFileDialogFile.FileName;

            bool isFileChecked = radioButtonFile.Checked;

            new Task(() => {
                Thread.Sleep(1000);
                bool error = false;
                try
                {
                    stegoSystem = new StegoSystem.StegoSystem()
                        .SetOffset(offset)
                        .SetMaxDiffusion(maxDiffusion)
                        .SetContainer(new Bitmap(imgName))
                        .SetHashSum(hashSum)
                        .SetCompression(compression)
                        .SetEncryption(encryption)
                        .SetTryForceEmbed(force);

                    if (isFileChecked)
                    {
                        stegoSystem.HideFile(fileName);
                    }
                    else
                    {
                        stegoSystem.HideString(textBoxText.Text, encoding);
                    }

                } catch(Exception ex)
                {
                    Invoke((MethodInvoker)(() =>
                    {
                        //MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }));

                    error = true;
                }

                Invoke((MethodInvoker)UnBlockUIAndHideProgress);

                if(error)
                {
                    Invoke((MethodInvoker)(() =>
                    {
                        buttonSaveImage.Enabled = false;
                        buttonSaveKey.Enabled = false;
                    }));
                }
                else
                {
                    Invoke((MethodInvoker)(() =>
                    {
                        MessageBox.Show("Data successfully integrated.\nDo not forget to save the image and the key!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SaveImage();
                        SaveKey();
                    }));
                }
            }).Start();
            
        }

        private void buttonSaveImage_Click(object sender, EventArgs e)
        {
            SaveImage();
        }
        private void SaveImage()
        {
            if (stegoSystem != null)
            {
                saveFileDialogImage.Title = "Save Image";
                saveFileDialogImage.ValidateNames = true;
                saveFileDialogImage.Filter = "Image (*.png)|*.png";
                saveFileDialogImage.FileName = "image.png";
                if (saveFileDialogImage.ShowDialog() == DialogResult.OK)
                {
                    stegoSystem.SaveResultBitmap(saveFileDialogImage.FileName);
                }
            }
        }

        private void buttonSaveKey_Click(object sender, EventArgs e)
        {
            SaveKey();
        }
        private void SaveKey()
        {
            if (stegoSystem != null)
            {
                saveFileDialogKey.Title = "Save Stegokey";
                saveFileDialogKey.ValidateNames = true;
                saveFileDialogKey.Filter = "Key (*.key)|*.key";
                saveFileDialogKey.FileName = "stego.key";
                if (saveFileDialogKey.ShowDialog() == DialogResult.OK)
                {
                    stegoSystem.SaveStegoKey(saveFileDialogKey.FileName);
                }

            }
        }
        private void FormEmbed_VisibleChanged(object sender, EventArgs e)
        {
            if (stegoSystem != null)
                stegoSystem.Dispose();

            buttonSaveImage.Enabled = false;
            buttonSaveKey.Enabled = false;
        }
    }
}
