; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{C232B9C4-F0CD-499C-94C0-927FBB47EE67}
AppName=AStego
AppVersion=1.0
;AppVerName=AStego 1.0
AppPublisher=Yegorov A.
AppPublisherURL=https://gitlab.com/Yegorov/StegoSystem
AppSupportURL=https://gitlab.com/Yegorov/StegoSystem
AppUpdatesURL=https://gitlab.com/Yegorov/StegoSystem
DefaultDirName={pf}\AStego
DisableProgramGroupPage=yes
LicenseFile=C:\Users\Admin\Desktop\StegoSystem\Files\LICENSE
InfoBeforeFile=C:\Users\Admin\Desktop\StegoSystem\Files\README
InfoAfterFile=C:\Users\Admin\Desktop\StegoSystem\Files\README_BEFORE
OutputDir=C:\Users\Admin\Desktop\StegoSystem\Files
OutputBaseFilename=astego_setup
Compression=lzma
SolidCompression=yes

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; OnlyBelowVersion: 0,6.1

[Files]
Source: "C:\Users\Admin\Desktop\StegoSystem\StegoGUI\bin\Debug\StegoGUI.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Admin\Desktop\StegoSystem\StegoGUI\bin\Debug\StegoSystem.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Admin\Desktop\StegoSystem\Files\User guide.pdf"; DestDir: "{app}"; Flags: isreadme
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{commonprograms}\AStego"; Filename: "{app}\StegoGUI.exe"
Name: "{commondesktop}\AStego"; Filename: "{app}\StegoGUI.exe"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\AStego"; Filename: "{app}\StegoGUI.exe"; Tasks: quicklaunchicon

[Run]
Filename: "{app}\StegoGUI.exe"; Description: "{cm:LaunchProgram,AStego}"; Flags: nowait postinstall skipifsilent

[Code]
// See this link https://blogs.msdn.microsoft.com/davidrickard/2015/07/17/installing-net-framework-4-5-automatically-with-inno-setup/
// https://msdn.microsoft.com/en-us/library/hh925568(v%3Dvs.110).aspx

procedure ExitProcess(exitCode:integer);
  external 'ExitProcess@kernel32.dll stdcall';

function Framework45IsNotInstalled(): Boolean;
var
  bSuccess: Boolean;
  regVersion: Cardinal;
begin
  Result := True;

  bSuccess := RegQueryDWordValue(HKLM, 'Software\Microsoft\NET Framework Setup\NDP\v4\Full', 'Release', regVersion);
  if (True = bSuccess) and (regVersion >= 378389) then begin
    Result := False;
  end;
end; 

procedure InitializeWizard;
begin
  if Framework45IsNotInstalled() then
  begin
    MsgBox('.NET Framework �� ����������, ���������� ���������� ��� � ��������� ������ ����������� ���������.', mbError, MB_OK);
    ExitProcess(0); // or WizardForm.Close;
    //idpAddFile('http://go.microsoft.com/fwlink/?LinkId=397707', ExpandConstant('{tmp}\NetFrameworkInstaller.exe'));
    //idpDownloadAfter(wpReady);
  end;
end;



