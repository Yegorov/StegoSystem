﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StegoSystem.DataTransform;
using System.Text;
using System.IO;

namespace UnitTest
{
    [TestClass]
    public class UnitTestEncryption
    {
        [TestMethod]
        public void TestMethodEncryption()
        {
            this.TestEncryption(new NoneEncryption());
            this.TestEncryption(new AES128Encryption());
        }

        public void TestEncryption(ISymmetricEncryption e)
        {
            ISymmetricEncryption enc = e;

            String inS = "Проверка работы шифрования текста";
            byte[] bIn = Encoding.UTF8.GetBytes(inS);

            byte[] bOut = enc.GetBytes(bIn, DataMode.IN);
            byte[] key = enc.GetKey();
            byte[] iv = enc.GetIV();

            enc.SetKeyIV(key, iv);
            byte[] bOut2 = enc.GetBytes(bOut, DataMode.OUT);
            String outS = Encoding.UTF8.GetString(bOut2);

            CollectionAssert.AreEqual(bIn, bOut2);
            StringAssert.Equals(inS, outS);


            MemoryStream ms = new MemoryStream(bIn);
            MemoryStream outs = new MemoryStream();
            ISymmetricEncryption sym = e;
            //sym.SetKeyIV(key, iv); // Нельзя указать ключи и вектор для шифрования, он генерируется всегда новый
            Stream cryptoStream = sym.GetStream(ms, outs, DataMode.IN);
            cryptoStream.Close();
            byte[] outBytes = outs.ToArray();

            MemoryStream nms = new MemoryStream(outBytes);
            MemoryStream outs2 = new MemoryStream();
            cryptoStream = sym.GetStream(nms, outs2, DataMode.OUT);
            cryptoStream.Close();
            byte[] out2Bytes = outs2.ToArray();

            CollectionAssert.AreEqual(bIn, out2Bytes);

        }
    }
}
