﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using StegoSystem.StegoTransform;
using StegoSystem;
using System.Drawing.Imaging;
using StegoSystem.Data;
using StegoSystem.DataTransform;
using System.Text;

namespace UnitTest
{
    [TestClass]
    public class UnitTestStegoTransform
    {
        [TestMethod]
        public void TestMethodStegoTransform()
        {
            byte[] expectedBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 7, 7, 10, 11, 12, 13, 14 };
            Bitmap bitmap = new Bitmap(@"C:\Users\Admin\Desktop\StegoSystem\Files\binaryExample.bmp");
            StegoEmbed se = new StegoEmbed();
            StegoKey sk = new StegoKey();
            Bitmap res = se.EmbedFromBytes(2000, 250, expectedBytes, bitmap, sk);
            //res.Save(@"C:\Users\Admin\Desktop\IMAG1442_res.png", ImageFormat.Png);

            StegoExtract ex = new StegoExtract();
            byte[] actualBytes = ex.ExtractToBytes(res, sk);

            CollectionAssert.AreEqual(expectedBytes, actualBytes);

        }

        [TestMethod]
        public void TestMethodStegoWithDataTransform()
        {
            String expectedText = "Проверка внедрения";

            IData data = new Text(expectedText);
            data.GetHeader().SetProperty(HeaderProperty.ENCODE, TypeTextEncoding.UTF_8);

            IDataTransformCoder coder = new DataCoder(new MD5HashSum(), new DeflateCompression(), new AES128Encryption());

            byte[] expectedBytes = coder.TransfromToBytes(data);

            StegoKey key = coder.GetStegoKey();

            Bitmap container = new Bitmap(@"C:\Users\Admin\Desktop\StegoSystem\Files\test100x100.png");
            int countPixelsInContainer = container.Height * container.Width;

            int offset = 10;
            int maxDiffusion = 100;
            int sizeEmbedData = expectedBytes.Length;

            int calculateSize = StegoCalculateForEmbed.CalculateMaxPixelsForEmbedFromString(EmbededModeValueConst.ONE_BYTE_POINTER_TWO_LEAST_BIT, offset, maxDiffusion, sizeEmbedData);

            if (calculateSize > countPixelsInContainer)
                throw new Exception("Не возможно внедрить данные в контейнер (мало места)");

            IStegoTransformEmbed embed = StegoTransformEmbedFactory.CreateFromString(EmbededModeValueConst.ONE_BYTE_POINTER_TWO_LEAST_BIT);

            Bitmap result = embed.EmbedFromBytes(offset, maxDiffusion, expectedBytes, container, key);
            //result.Save(@"C:\Users\Admin\Desktop\test_100_4.png");

            IStegoTransformExtract extract = StegoTransformExtractFactory.CreateFromNumber(key.EmbededMode);

            byte[] actualBytes = extract.ExtractToBytes(result, key);

            IDataTransformEncoder encoder = new DataEncoder(key);
            byte[] realBytes = encoder.TransfromFromBytes(actualBytes);
            IHeader header = encoder.GetHeader();

            if ((TypeFile)header.GetProperty(HeaderProperty.TYPE_FILE) == TypeFile.NONE)
            {
                String encoding = ((TypeTextEncoding)header.GetProperty(HeaderProperty.ENCODE)).Name;
                String actualText = Encoding.GetEncoding(encoding).GetString(realBytes);

                StringAssert.Equals(expectedText, actualText);
            }
        }
    }
}
