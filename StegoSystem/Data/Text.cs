﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.Data
{
    public class Text : IData
    {
        private String tData;
        private TextHeader tHeader;
        private MemoryStream tStream;
        private byte[] tBytes;

        public Text(String tData)
        {
            this.tData = tData;
            this.tHeader = new TextHeader();
            tBytes = null;
        }
        public void ChangeTextEncoding(TypeTextEncoding textEncoding)
        {
            tHeader.SetProperty(HeaderProperty.ENCODE, textEncoding);
            tData = null;
            if (tStream != null)
            {
                tStream.Close();
                tStream = null;
            }
        }
        public byte[] GetBytes()
        {
            TypeTextEncoding textEncoding = (TypeTextEncoding)tHeader.GetProperty(HeaderProperty.ENCODE);
            tBytes = Encoding.GetEncoding(textEncoding.Name, 
                                            new EncoderExceptionFallback(),
                                            new DecoderExceptionFallback()).GetBytes(tData);
  
            return tBytes;
        }

        public IHeader GetHeader()
        {
            return tHeader;
        }

        public int GetSize()
        {
            this.GetBytes();
            return tBytes.Length;
        }

        public Stream GetStream()
        {
            if(tStream != null)
            {
                tStream.Close();
                this.GetBytes();
                tStream = new MemoryStream(tBytes);
            }
            return tStream;
        }
    }
}
