﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.Data
{
    public class TextHeader : IHeader
    {
        private TypeFile typeFile; // 1 byte
        private byte offsetData;  // 1 byte
        private uint sizeText; // 4 byte

        private TypeTextEncoding typeTextEncoding; // 1 byte

        private CompressMode compressMode; // 1 byte

        private TypeHashSum typeHashSum; // 1 byte
        private byte[] valueHashSum; // n bytes

        // Размеры полей в байтах
        private static readonly int SIZEOF_TYPE_FILE = 1;
        private static readonly int SIZEOF_OFFSET_DATA = 1;
        private static readonly int SIZEOF_SIZE_TEXT = 4;
        private static readonly int SIZEOF_TYPE_TEXT_ENCODING = 1;
        private static readonly int SIZEOF_COMPRESS_MODE = 1;
        private static readonly int SIZEOF_TYPE_HASH_SUM = 1;

        public TextHeader()
        {
            this.typeFile = TypeFile.NONE;
            offsetData = (byte)GetOnlyConstSize();
            sizeText = 0;
            typeTextEncoding = TypeTextEncoding.UTF_8;
            compressMode = CompressMode.NONE;
            typeHashSum = TypeHashSum.NONE;
            valueHashSum = null;
        }

        public static TextHeader CreateTextHeaderFromBytes(byte[] bytes)
        {
            TextHeader th = new TextHeader();

            //th.SetProperty(HeaderProperty.TYPE_FILE, TypeFile.GetFileType(bytes[0]));
            th.SetProperty(HeaderProperty.OFFSET_DATA, bytes[1]);
            th.SetProperty(HeaderProperty.SIZE_FILE, BitConverter.ToUInt32(bytes.Skip(2).Take(SIZEOF_SIZE_TEXT).ToArray(), 0));
            th.SetProperty(HeaderProperty.ENCODE, TypeTextEncoding.GetTypeTextEncoding(bytes[6]));
            th.SetProperty(HeaderProperty.COMPRESSION_MODE, CompressMode.GetCompressMode(bytes[7]));

            TypeHashSum tHashSum = TypeHashSum.GetTypeHashSum(bytes[8]);
            th.SetProperty(HeaderProperty.TYPE_HASH_SUM, tHashSum);
            if (tHashSum != TypeHashSum.NONE)
            {
                byte[] hashSum = bytes.Skip(9).Take(tHashSum.SizeInBytes).ToArray();
                th.SetProperty(HeaderProperty.VALUE_HASH_SUM, hashSum);
            }

            return th;
        }

        public byte[] GetBytes()
        {
            int allSizeBytes = this.GetOnlyConstSize();
            byte[] bytes = new byte[allSizeBytes + typeHashSum.SizeInBytes];

            bytes[0] = (byte)typeFile.Value; // 0
            bytes[1] = (byte)(allSizeBytes + typeHashSum.SizeInBytes); // 1
            Array.Copy(BitConverter.GetBytes(sizeText), 0, bytes, 2, SIZEOF_SIZE_TEXT); // 2, 3, 4, 5
            bytes[6] = (byte)typeTextEncoding.Value; // 6
            bytes[7] = (byte)compressMode.Value; // 7
            bytes[8] = (byte)typeHashSum.Value; // 8

            if (typeHashSum != TypeHashSum.NONE)
                Array.Copy(valueHashSum, 0, bytes, 9, typeHashSum.SizeInBytes); // 9 ... 9 + typeHashSum.Size - 1

            return bytes;
        }

        public int GetOnlyConstSize()
        {
            return SIZEOF_TYPE_FILE + SIZEOF_OFFSET_DATA + SIZEOF_SIZE_TEXT + SIZEOF_TYPE_TEXT_ENCODING +
                    SIZEOF_COMPRESS_MODE + SIZEOF_TYPE_HASH_SUM;
        }

        public object GetProperty(HeaderProperty property)
        {
            switch (property)
            {
                case HeaderProperty.TYPE_FILE:
                    {
                        return typeFile;
                        break;
                    }
                case HeaderProperty.OFFSET_DATA:
                    {
                        return offsetData;
                        break;
                    }
                case HeaderProperty.SIZE_FILE:
                    {
                        return sizeText;
                        break;
                    }
                case HeaderProperty.ENCODE:
                    {
                        return typeTextEncoding;
                        break;
                    }
                case HeaderProperty.COMPRESSION_MODE:
                    {
                        return compressMode;
                        break;
                    }
                case HeaderProperty.TYPE_HASH_SUM:
                    {
                        return typeHashSum;
                        break;
                    }
                case HeaderProperty.VALUE_HASH_SUM:
                    {
                        return valueHashSum;
                        break;
                    }
            }
            return null;
        }

        public void SetProperty(HeaderProperty property, object value)
        {
            switch (property)
            {
                case HeaderProperty.TYPE_FILE:
                    {
                        //typeFile = (TypeFile)value;
                        // Для текста нельзя установить формат (расширение файла)
                        break;
                    }
                case HeaderProperty.OFFSET_DATA:
                    {
                        offsetData = (byte)value;
                        break;
                    }
                case HeaderProperty.SIZE_FILE:
                    {
                        sizeText = (uint)value;
                        break;
                    }
                case HeaderProperty.ENCODE:
                    {
                        typeTextEncoding = (TypeTextEncoding)value;
                        break;
                    }
                case HeaderProperty.COMPRESSION_MODE:
                    {
                        compressMode = (CompressMode)value;
                        break;
                    }
                case HeaderProperty.TYPE_HASH_SUM:
                    {
                        typeHashSum = (TypeHashSum)value;
                        break;
                    }
                case HeaderProperty.VALUE_HASH_SUM:
                    {
                        valueHashSum = (byte[])value;
                        break;
                    }
            }
        }
    }
}
