﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StegoSystem.DataTransform;
using StegoSystem.Data;
using System.Text;
using StegoSystem;

namespace UnitTest
{
    [TestClass]
    public class UnitTestDataTransform
    {
        [TestMethod]
        public void TestDataTransformCoderEncoderText()
        {
            String text = "Прооооверка текста для шифрования и других преобразований!!!!!!!!!!!!!";
            IDataTransformCoder cod = new DataCoder(new SHA_256HashSum(), new GZipCompression(), new AES128Encryption());
            IData d = new Text(text);
            byte[] bytesForStegoTransform = cod.TransfromToBytes(d);

            IDataTransformEncoder enc = new DataEncoder(cod.GetStegoKey());
            byte[] bytes = enc.TransfromFromBytes(bytesForStegoTransform);

            IHeader header = enc.GetHeader();
            CollectionAssert.AreEqual(d.GetBytes(), bytes);
            try
            {
                String encoding = ((TypeTextEncoding)(header.GetProperty(HeaderProperty.ENCODE))).Name;
                String s = Encoding.GetEncoding(encoding).GetString(bytes);
                
                StringAssert.Equals(text, s);
            }
            catch (NullReferenceException)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestDataTransformCoderEncoderFile()
        {
            IDataTransformCoder coder = new DataCoder(new MD5HashSum(), new GZipCompression(), new AES128Encryption());
            //IDataTransformCoder coder = new DataCoder(new NoneHashSum(), new NoneCompression(), new NoneEncryption());
            IData data = new StegoSystem.Data.File(@"C:\Users\Admin\Desktop\StegoSystem\Files\test.txt");
            data.GetHeader().SetProperty(HeaderProperty.ENCODE, TypeTextEncoding.UTF_8); // игнорируется, потому что файл, а текст
            byte[] bytesForStegoTransform = coder.TransfromToBytes(data);

            StegoKey stegoKey = coder.GetStegoKey();
            String stegoKeyBase64String = stegoKey.ToBase64String();
            StegoKey newStegoKey = StegoKey.CreateFromBase64String(stegoKeyBase64String);

            Assert.AreEqual(stegoKey, newStegoKey);


            IDataTransformEncoder encoder = new DataEncoder(newStegoKey);
            byte[] fileBytes = encoder.TransfromFromBytes(bytesForStegoTransform);
            IHeader header = encoder.GetHeader();

            CollectionAssert.AreEqual(data.GetBytes(), fileBytes);
        }
    }
}
