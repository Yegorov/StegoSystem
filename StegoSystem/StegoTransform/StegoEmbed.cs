﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.StegoTransform
{
    // Внедрение
    public class StegoEmbed : IStegoTransformEmbed
    {
        private static readonly int MAX_LIMIT = 250;
        private static readonly EmbededModeValue embededModeValue = EmbededModeValue.ONE_BYTE_POINTER_TWO_LEAST_BIT;

        public Bitmap EmbedFromBytes(int offset, int maxDiffusion, byte[] bytes, Bitmap container, StegoKey key)
        {

            // 0 < maxDiffusion < MAX_LIMIT 
            // 255 - конец указателя и нужно прочитать первый байт, второй отбросить (для нечетного кол-во байтов)
            // 254 - конец указателя и нужно прочитать два байта (для четного кол-ва байтов)
            // любое другое число < maxDiffusion является указателем (смещением на следующую четвертку пикселей) на следующие данные

            if (offset < 0)
                throw new ArgumentException("Аргумент offset не должен быть отрицательным");
            if (maxDiffusion < 0)
                throw new ArgumentException("Аргумент maxDiffusion не должен быть отрицательным");
            if (maxDiffusion > MAX_LIMIT)
                throw new ArgumentException("Аргумент maxDiffusion не должен быть больше " + MAX_LIMIT);

            // Заполняем стегоключ
            key.EmbededMode = (byte)embededModeValue;
            key.Offset = (uint)offset;

            Bitmap resultImage = new Bitmap(container.Width, container.Height);

            // Переводим Bitmap в поток 
            int sizeColorBytesInContainer = container.Width * container.Height * 4;
            Stream srcStream = new MemoryStream(sizeColorBytesInContainer);
            BinaryWriter srcStreamWriter = new BinaryWriter(srcStream);
            BinaryReader srcStreamReader = new BinaryReader(srcStream);

            BitmapToStream(srcStreamWriter, container);
            srcStream.Seek(0, SeekOrigin.Begin);

            EmbedStream(offset, maxDiffusion, bytes, srcStreamWriter, srcStreamReader);

            srcStream.Seek(0, SeekOrigin.Begin);
            StreamToBitmap(srcStreamReader, resultImage);

            srcStreamWriter.Close();
            srcStreamReader.Close();

            return resultImage;

        }

        private void EmbedStream(int offset, int maxDiffusion, byte[] bytes, BinaryWriter streamWriter, BinaryReader streamReader)
        {
            Random rand = new Random(System.DateTime.Now.Millisecond % (offset + 1));
            
            streamReader.BaseStream.Seek(offset * 4, SeekOrigin.Begin);

            int currentPair = 0;
            Color color1, color2, color3, color4;

            int bytesLength = bytes.Length; // Получаем длину данных в байтах для внедрения
            if (bytes.Length % 2 == 0)      // Если длина четная
                bytesLength = bytesLength - 2; // то вычитаем последние два байта
            else
                bytesLength = bytesLength - 1; // вычитаем один последний байт, 
            // для последних двух (одного) байта своя процедура обработки

            byte pointer;
            byte data1;
            byte data2;
            while (currentPair < bytesLength)
            {
                if (streamReader.BaseStream.Position > streamReader.BaseStream.Length - 4 * 4)
                {
                    streamReader.Close();
                    streamWriter.Close();
                    throw new Exception("Не возможно внедрить данные в изображения! (Уменьшите данные или загрузите больший размер изображения контейнера)");
                }

                color1 = Color.FromArgb(streamReader.ReadInt32());
                color2 = Color.FromArgb(streamReader.ReadInt32());
                color3 = Color.FromArgb(streamReader.ReadInt32());
                color4 = Color.FromArgb(streamReader.ReadInt32());

                pointer = (byte)rand.Next(maxDiffusion);
                data1 = bytes[currentPair];
                data2 = bytes[currentPair + 1];
                                                // обнулить 2    Разбить байт по 2 бита
                                                // последних     и поставить на свое место
                                                // бита
                color1 = Color.FromArgb((byte)(color1.R & 252 | ((pointer & 255) >> 6)),
                                        (byte)(color1.G & 252 | ((pointer & 63)  >> 4)),
                                        (byte)(color1.B & 252 | ((pointer & 15)  >> 2)));

                color2 = Color.FromArgb((byte)(color2.R & 252 | ((pointer & 3)   >> 0)),
                                        (byte)(color2.G & 252 | ((data1 & 255)   >> 6)),
                                        (byte)(color2.B & 252 | ((data1 & 63)    >> 4)));

                color3 = Color.FromArgb((byte)(color3.R & 252 | ((data1 & 15)    >> 2)),
                                        (byte)(color3.G & 252 | ((data1 & 3)     >> 0)),
                                        (byte)(color3.B & 252 | ((data2 & 255)   >> 6)));

                color4 = Color.FromArgb((byte)(color4.R & 252 | ((data2 & 63)    >> 4)),
                                        (byte)(color4.G & 252 | ((data2 & 15)    >> 2)),
                                        (byte)(color4.B & 252 | ((data2 & 3)     >> 0)));


                streamReader.BaseStream.Seek(-4 * 4, SeekOrigin.Current); // Back
                streamWriter.Write(color1.ToArgb());
                streamWriter.Write(color2.ToArgb());
                streamWriter.Write(color3.ToArgb());
                streamWriter.Write(color4.ToArgb());


                streamReader.BaseStream.Seek(pointer * 4, SeekOrigin.Current); // Переместить поток на заданной кол-во пикселей
                currentPair += 2; // Переместить номер текущуй пары внедряемых данных
            }

            // Процедура обработки последних двух (одного) байта
            if (bytes.Length % 2 == 0)      // Если длина четная
            {
                pointer = 254;
                data1 = bytes[bytes.Length - 2];
                data2 = bytes[bytes.Length - 1];
            }
            else
            {
                pointer = 255;
                data1 = bytes[bytes.Length - 1];
                data2 = 42;
            }

            color1 = Color.FromArgb(streamReader.ReadInt32());
            color2 = Color.FromArgb(streamReader.ReadInt32());
            color3 = Color.FromArgb(streamReader.ReadInt32());
            color4 = Color.FromArgb(streamReader.ReadInt32());

            color1 = Color.FromArgb((byte)(color1.R & 252 | ((pointer & 255) >> 6)),
                                    (byte)(color1.G & 252 | ((pointer & 63) >> 4)),
                                    (byte)(color1.B & 252 | ((pointer & 15) >> 2)));

            color2 = Color.FromArgb((byte)(color2.R & 252 | ((pointer & 3) >> 0)),
                                    (byte)(color2.G & 252 | ((data1 & 255) >> 6)),
                                    (byte)(color2.B & 252 | ((data1 & 63) >> 4)));

            color3 = Color.FromArgb((byte)(color3.R & 252 | ((data1 & 15) >> 2)),
                                    (byte)(color3.G & 252 | ((data1 & 3) >> 0)),
                                    (byte)(color3.B & 252 | ((data2 & 255) >> 6)));

            color4 = Color.FromArgb((byte)(color4.R & 252 | ((data2 & 63) >> 4)),
                                    (byte)(color4.G & 252 | ((data2 & 15) >> 2)),
                                    (byte)(color4.B & 252 | ((data2 & 3) >> 0)));


            streamReader.BaseStream.Seek(-4 * 4, SeekOrigin.Current); // Back
            streamWriter.Write(color1.ToArgb());
            streamWriter.Write(color2.ToArgb());
            streamWriter.Write(color3.ToArgb());
            streamWriter.Write(color4.ToArgb());
        }

        private void BitmapToStream(BinaryWriter streamWriter, Bitmap bitmap)
        {
            Color color;
            for (int y = 0; y < bitmap.Height; ++y)
            {
                for (int x = 0; x < bitmap.Width; ++x)
                {
                    color = bitmap.GetPixel(x, y);
                    streamWriter.Write(color.ToArgb());
                }
            }
        }

        private void StreamToBitmap(BinaryReader streamReader, Bitmap bitmap)
        {
            Color color;
            for (int y = 0; y < bitmap.Height; ++y)
            {
                for (int x = 0; x < bitmap.Width; ++x)
                {
                    color = Color.FromArgb(streamReader.ReadInt32());
                    bitmap.SetPixel(x, y, color);
                }
            }
        }

        public Bitmap EmbedFromStream(int offset, int maxDiffusion, Stream stream,  Bitmap container, StegoKey key)
        {
            throw new NotImplementedException();
        }

        public static int CalculateMaxPixelsForEmbed(int offset, int maxDiffusion, int sizeEmbedData)
        {
            // sizeEmbedData - кол-во байт после всех преобразований данных (DataTransform pipeline)

            if (offset < 0)
                throw new ArgumentException("Аргумент offset не должен быть отрицательным");
            if (maxDiffusion < 0)
                throw new ArgumentException("Аргумент maxDiffusion не должен быть отрицательным");
            if (sizeEmbedData < 0)
                throw new ArgumentException("Аргумент sizeEmbedData не должен быть отрицательным");

            // 2 байта на данные      все это занимает 4 пикселя в изображении 
            // 1 байт на указатель    (RGB,RGB,RGB,RGB (4 * 3) кол-во всего каналов * 2 последних бита  = 24 бита = 3 байта)
            // 

            // Следовательно можно составить пропорцию:
            // 2 байта         -    4 пикселя
            //                                              => P = sizeEmbedData * 4 / 2 = 2 * sizeEmbedData
            // sizeEmbedData   -    P (пикселей всего)
            // Это первая часть в формуле (в скобках)

            // maxDiffusion - Максимальное число пикселей, которое нужно пропустить, 
            //                чтобы получить следующую четвертку пикселей
            //                со встроенными данными
            // 
            // sizeEmbedData / 2 - количество пар, минус 1, так как нужны промежутки между ними |..|..|..| (например точки)

            if (sizeEmbedData % 2 == 0) // Если кол- во внедряемых байт четное 
                return offset + (2 * sizeEmbedData + (sizeEmbedData / 2 - 1) * maxDiffusion);
            else // Если кол- во внедряемых байт нечетное добавить 1 байт ;) 
                return offset + (2 * (sizeEmbedData + 1) + ((sizeEmbedData + 1) / 2 - 1) * maxDiffusion);
  

        }
    }

    public static class StegoTransformEmbedFactory
    {
        public static IStegoTransformEmbed CreateFromString(String nameStego)
        {
            if (nameStego == EmbededModeValueConst.ONE_BYTE_POINTER_TWO_LEAST_BIT)
                return new StegoEmbed();

            throw new Exception("Не найден алгоритм для внедрения данных");
        }
    }

    public static class StegoCalculateForEmbed
    {
        public static int CalculateMaxPixelsForEmbedFromString(String nameStego, int offset, int maxDiffusion, int sizeEmbedData)
        {
            if (nameStego == EmbededModeValueConst.ONE_BYTE_POINTER_TWO_LEAST_BIT)
                return StegoEmbed.CalculateMaxPixelsForEmbed(offset, maxDiffusion, sizeEmbedData);

            throw new Exception("Не найден алгоритм для внедрения данных");
        }

    }
}
