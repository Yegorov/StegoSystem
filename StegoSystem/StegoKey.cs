﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem
{
    public class StegoKey
    {
        public UInt32 Offset { get; set; } // Первое смещение начала чтения (кол-во писелей от начала) 
        public Byte EmbededMode { get; set; } // Режим встраивания данных в изображение
        public Byte TypeEncryption { get; set; } // Тип симметричного алгоритма шифрования (1 - AES,...)

        public UInt16 SizeEncryptionIV { get; private set; } // Размер вектора инициализации
        public UInt16 SizeEncryptionKey { get; private set; } // Размер ключа шифрования
        public Byte[] IV { get; private set; } // Вектор инициализации
        public Byte[] Key { get; private set; } // Ключ шифрования

        // Размеры полей в байтах
        private static readonly int SIZEOF_OFFSET = 4;
        private static readonly int SIZEOF_EMBEDED_MODE = 1;
        private static readonly int SIZEOF_TYPE_ENCRYPTION = 2;
        private static readonly int SIZEOF_SIZE_ENCRYPTION_IV = 2;
        private static readonly int SIZEOF_SIZE_ENCRYPTION_KEY = 1;

        public StegoKey(UInt32 offset, Byte embededMode, Byte typeEncryption, Byte[] iv, Byte[] key)
        {
            this.Offset = offset;
            this.EmbededMode = embededMode;
            this.TypeEncryption = typeEncryption;
            this.SetIV(iv);
            this.SetKey(key);
        }

        public StegoKey()
        {
            IV = new byte[] { 0 };
            SizeEncryptionIV = 1;
            Key = new byte[] { 0 };
            SizeEncryptionKey = 1;
        }

        public void SetIV(byte[] iv)
        {
            if (iv == null)
                throw new ArgumentNullException("Параметр iv не должен быть равным null");

            if (iv.Length > UInt16.MaxValue)
                throw new ArgumentOutOfRangeException("Размер вектора инициализиции не должен превышать " + UInt16.MaxValue + " байт");

            IV = iv;
            SizeEncryptionIV = (UInt16)IV.Length;
        }

        public void SetKey(byte[] key)
        {
            if (key == null)
                throw new ArgumentNullException("Параметр key не должен быть равным null");

            if(key.Length > UInt16.MaxValue)
                throw new ArgumentOutOfRangeException("Размер ключа шифрования не должен превышать " + UInt16.MaxValue + " байт");

            //or Array.Copy
            Key = key;
            SizeEncryptionKey = (UInt16)key.Length;
        }

        public byte[] ToBytes()
        {
            int countBytes = SIZEOF_OFFSET + SIZEOF_EMBEDED_MODE + SIZEOF_TYPE_ENCRYPTION +
                                SIZEOF_SIZE_ENCRYPTION_IV + IV.Length +
                                SIZEOF_SIZE_ENCRYPTION_KEY + Key.Length;
            byte[] bytes = new byte[countBytes];

            Array.Copy(BitConverter.GetBytes(Offset), bytes, SIZEOF_OFFSET); // 0, 1, 2, 3
            bytes[4] = EmbededMode;       // 4
            bytes[5] = TypeEncryption;    // 5
            Array.Copy(BitConverter.GetBytes(SizeEncryptionIV), 0, bytes, 6, SIZEOF_SIZE_ENCRYPTION_IV);   // 6, 7
            Array.Copy(BitConverter.GetBytes(SizeEncryptionKey), 0, bytes, 8, SIZEOF_SIZE_ENCRYPTION_KEY); // 8, 9
            Array.Copy(IV, 0, bytes, 10, SizeEncryptionIV); // 10, ... (10 + SizeEncryptionIV - 1)
            Array.Copy(Key, 0, bytes, 10 + SizeEncryptionIV, SizeEncryptionKey); // 10 + SizeEncryptionIV, ... (10 + SizeEncryptionIV + SizeEncryptionKey - 1)

            return bytes;
        }
        public String ToBase64String()
        {
            byte[] bytes = this.ToBytes();
            return Convert.ToBase64String(bytes, Base64FormattingOptions.InsertLineBreaks);
        }
        public static StegoKey CreateFromBytes(byte[] bytes)
        {
            UInt16 offset = BitConverter.ToUInt16(bytes.Take(4).ToArray(), 0);
            Byte embededMode = bytes[4];
            Byte typeEncryption = bytes[5];
            ushort sizeIV = BitConverter.ToUInt16(bytes.Skip(6).Take(2).ToArray(), 0);
            ushort sizeKey = BitConverter.ToUInt16(bytes.Skip(8).Take(2).ToArray(), 0);
            byte[] iv = bytes.Skip(10).Take(sizeIV).ToArray();
            byte[] key = bytes.Skip(10 + sizeIV).Take(sizeKey).ToArray();
            return new StegoKey(offset, embededMode, typeEncryption, iv, key);
        }

        public static StegoKey CreateFromBase64String(String base64String)
        {
            byte[] bytes = Convert.FromBase64String(base64String);
            return StegoKey.CreateFromBytes(bytes);
        }

        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            StegoKey k = obj as StegoKey;
            if (k == null)
            {
                return false;
            }

            return (this.Offset == k.Offset) &&
                   (this.EmbededMode == k.EmbededMode) &&
                   (this.TypeEncryption == k.TypeEncryption) &&
                   (this.SizeEncryptionIV == k.SizeEncryptionIV) &&
                   (this.SizeEncryptionKey == k.SizeEncryptionKey) &&
                   (this.IV.SequenceEqual(k.IV)) &&
                   (this.Key.SequenceEqual(k.Key));
        }
    }

    public enum EmbededModeValue
    {
        ONE_BYTE_POINTER_TWO_LEAST_BIT = 1
        //, ...
    }
    public static class EmbededModeValueConst
    {
        public static readonly String ONE_BYTE_POINTER_TWO_LEAST_BIT = "ONE_BYTE_POINTER_TWO_LEAST_BIT";
    }


    public enum TypeEncryptionValue
    {
        NONE = 0,
        AES_128 = 1,
        TRIPLE_DES_128,
        //, ...
    }

    public static class TypeEncryptionValueConst
    {
        public static readonly String NONE = "";
        public static readonly String AES_128 = "AES-128";
        public static readonly String TRIPLE_DES_128 = "3DES-128";
    }
}
