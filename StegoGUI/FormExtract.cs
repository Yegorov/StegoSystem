﻿using StegoSystem;
using StegoSystem.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StegoGUI
{
    public partial class FormExtract : Form
    {
        private StegoSystem.StegoSystem stegoSystem;
        private String imageFileName;
        private String keyFileName;
        private String fileExt;
        public FormExtract()
        {
            InitializeComponent();
            imageFileName = "";
            keyFileName = "";

            buttonSave.Enabled = false;
            textBoxText.Enabled = false;

            labelProgress.Visible = false;
            pictureBoxProgress.Visible = false;
        }

        private void BlockUIAndShowProgress()
        {
            labelProgress.Visible = true;
            pictureBoxProgress.Visible = true;


            foreach (Control c in this.Controls)
            {
                if (c.Name == "labelProgress" || c.Name == "pictureBoxProgress") continue;
                c.Enabled = false;
            }
        }

        private void UnBlockUIAndHideProgress()
        {

            foreach (Control c in this.Controls)
            {
                if (c.Name == "buttonSave" || c.Name == "textBoxText") continue;
                c.Enabled = true;
            }
            labelProgress.Visible = false;
            pictureBoxProgress.Visible = false;
        }

        private void buttonSetImg_Click(object sender, EventArgs e)
        {
            openFileDialogImage.CheckFileExists = true;
            openFileDialogImage.Filter = "Image PNG (*.png)|*.png";
            if (openFileDialogImage.ShowDialog() == DialogResult.OK)
            {
                imageFileName = openFileDialogImage.FileName;
            }
            else
            {
                imageFileName = "";
            }

        }

        private void buttonSetKey_Click(object sender, EventArgs e)
        {
            openFileDialogKey.CheckFileExists = true;
            openFileDialogKey.Filter = "Key (*.key)|*.key";
            if (openFileDialogKey.ShowDialog() == DialogResult.OK)
            {
                keyFileName = openFileDialogKey.FileName;
            }
            else
            {
                keyFileName = "";
            }
        }

        private void buttonExtract_Click(object sender, EventArgs e)
        {
            if (stegoSystem != null)
                stegoSystem.Dispose();

            if (imageFileName.SequenceEqual(""))
            {
                MessageBox.Show("Image is not choosen!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (keyFileName.SequenceEqual(""))
            {
                MessageBox.Show("Key is not choosen!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            label1.Text = "";
            BlockUIAndShowProgress();
            new Task(() => {
                Thread.Sleep(1000);
                try
                {
                    stegoSystem = new StegoSystem.StegoSystem()
                        .SetContainer(new Bitmap(imageFileName))
                        .SetStegoKeyFromFile(keyFileName);

                    stegoSystem.ExtractBytesAndHeader();

                    IHeader h = stegoSystem.GetHeader();

                    StringBuilder sb = new StringBuilder("Header:\n");
                    TypeFile tf = (TypeFile)h.GetProperty(HeaderProperty.TYPE_FILE);
                    sb.AppendLine("Type File: " + tf.Name);
                    fileExt = tf.Name;

                    TypeHashSum ths = (TypeHashSum)h.GetProperty(HeaderProperty.TYPE_HASH_SUM);
                    sb.AppendLine("Type Hash Sum: " + ths.Name);
                    byte[] hashValue = (byte[])h.GetProperty(HeaderProperty.VALUE_HASH_SUM);
                    if (hashValue != null)
                    {
                        String strHash = BitConverter.ToString(hashValue);
                        sb.AppendLine("Value Hash Sum: " + strHash);
                    }

                    CompressMode cm = (CompressMode)h.GetProperty(HeaderProperty.COMPRESSION_MODE);
                    sb.AppendLine("Compress Mode: " + cm.Name);


                    if (h is TextHeader)
                    {
                        String encoding = ((TypeTextEncoding)h.GetProperty(HeaderProperty.ENCODE)).Name;
                        String text = Encoding.GetEncoding(encoding).GetString(stegoSystem.GetBytes());
                        sb.AppendLine("Text Encoding: " + encoding);

                        Invoke((MethodInvoker)(() =>
                        {
                            textBoxText.Text = text;

                            buttonSave.Enabled = false;
                            textBoxText.Enabled = true;

                            label1.Text = sb.ToString();
                        }));
                    }
                    else if (h is FileHeader)
                    {
                        Invoke((MethodInvoker)(() =>
                        {
                            buttonSave.Enabled = true;
                            textBoxText.Enabled = false;
                            label1.Text = sb.ToString();
                        }));
                    }
                }
                catch (Exception ex)
                {
                    Invoke((MethodInvoker)(() =>
                    {
                        //MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }));
                }

                Invoke((MethodInvoker)UnBlockUIAndHideProgress);

            }).Start();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (stegoSystem != null)
            {
                saveFileDialogSaveFile.ValidateNames = true;
                saveFileDialogSaveFile.Filter = String.Format("File (*.{0})|*.{0}", fileExt);
                saveFileDialogSaveFile.FileName = "file." + fileExt;
                if (saveFileDialogSaveFile.ShowDialog() == DialogResult.OK)
                {
                    System.IO.File.WriteAllBytes(saveFileDialogSaveFile.FileName, stegoSystem.GetBytes());
                }
            }
        }

        private void FormExtract_VisibleChanged(object sender, EventArgs e)
        {
            if (stegoSystem != null)
                stegoSystem.Dispose();

            imageFileName = "";
            keyFileName = "";

            label1.Text = "";
            textBoxText.Text = "";
        }
    }
}
