﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegoSystem.StegoTransform
{
    // Внедрение данных (Coder)
    public interface IStegoTransformEmbed
    {
        // Возвращает изображение со встроенными данными
        Bitmap EmbedFromBytes(int offset, int maxDiffusion, byte[] bytes, Bitmap container, StegoKey key); 
        Bitmap EmbedFromStream(int offset, int maxDiffusion, Stream stream, Bitmap container, StegoKey key);

        //void SetStegoKey(StegoKey key); // StegoKey должен быть в конструкторе
        //void SetBitmap(Bitmap bitmap); // Должен принимать в конструкторе Bitmap
        //StegoKey GetStegoKey(); // Возвращает заполненный стегоключ
        //Bitmap GetBitmap(); // Возвращает изображение со встроенными данными
    }

    // Извлечение данных (Encoder)
    public interface IStegoTransformExtract
    {
        byte[] ExtractToBytes(Bitmap bitmap, StegoKey key); // Извлечь данные (в массив байт) из изображения используя стего ключ
        Stream ExtractToStream(Bitmap bitmap, StegoKey key); // Извлечь данные (в поток) из изображения используя стего ключ
        //void SetStegoKey(StegoKey key); // StegoKey должен быть в конструкторе
        //void SetBitmap(Bitmap bitmap); // Должен принимать в конструкторе Bitmap
    }
}
