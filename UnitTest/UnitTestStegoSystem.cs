﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StegoSystem;
using System.Drawing;
using StegoSystem.Data;
using System.Text;

namespace UnitTest
{
    [TestClass]
    public class UnitTestStegoSystem
    {
        [TestMethod]
        public void TestMethodStegoSystem()
        {
            StegoSystem.StegoSystem s = new StegoSystem.StegoSystem()
                .SetOffset(20)
                .SetMaxDiffusion(250)
                .SetContainer(new Bitmap(@"C:\Users\Admin\Desktop\StegoSystem\Files\test100x100.png"))
                .SetHashSum("sha-256")
                .SetEncryption("AES-128")
                .SetCompression("deflate")
                .SetTryForceEmbed(true);

            s.HideString(@"Проверка работы алгоритмов :)", "utf-8");

            s.SaveResultBitmap("mytest.png");
            s.SaveStegoKey("mytest.key");
            s.Dispose();

            s = new StegoSystem.StegoSystem()
                .SetContainer(new Bitmap("mytest.png"))
                .SetStegoKeyFromFile("mytest.key");

            s.ExtractBytesAndHeader();

            IHeader h = s.GetHeader();
            if (h is TextHeader)
            {
                String encoding = ((TypeTextEncoding)h.GetProperty(HeaderProperty.ENCODE)).Name;
                String text = Encoding.GetEncoding(encoding).GetString(s.GetBytes());

            }
            s.Dispose();
            //VisualStegoAttack.ViewBit(new Bitmap("res2.png"), 3).Save("dstview.bmp");
            //VisualStegoAttack.ViewBit(new Bitmap(@"C:\Users\Admin\Desktop\IMAG1442.jpg"), 3).Save("srcview.bmp");
        }
    }
}
