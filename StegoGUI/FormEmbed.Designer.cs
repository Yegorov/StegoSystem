﻿namespace StegoGUI
{
    partial class FormEmbed
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonChooseImage = new System.Windows.Forms.Button();
            this.checkBoxForce = new System.Windows.Forms.CheckBox();
            this.openFileDialogImage = new System.Windows.Forms.OpenFileDialog();
            this.trackBarDiffusion = new System.Windows.Forms.TrackBar();
            this.radioButtonFile = new System.Windows.Forms.RadioButton();
            this.radioButtonText = new System.Windows.Forms.RadioButton();
            this.textBoxText = new System.Windows.Forms.TextBox();
            this.buttonEmbed = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxDiffusion = new System.Windows.Forms.TextBox();
            this.buttonChooseFile = new System.Windows.Forms.Button();
            this.labelFileName = new System.Windows.Forms.Label();
            this.comboBoxEncoding = new System.Windows.Forms.ComboBox();
            this.comboBoxStego = new System.Windows.Forms.ComboBox();
            this.labelImageName = new System.Windows.Forms.Label();
            this.labelStego = new System.Windows.Forms.Label();
            this.labelHash = new System.Windows.Forms.Label();
            this.comboBoxHashSum = new System.Windows.Forms.ComboBox();
            this.labelCompression = new System.Windows.Forms.Label();
            this.comboBoxCompression = new System.Windows.Forms.ComboBox();
            this.labelEncryption = new System.Windows.Forms.Label();
            this.comboBoxEncryption = new System.Windows.Forms.ComboBox();
            this.openFileDialogFile = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxOffset = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonSaveImage = new System.Windows.Forms.Button();
            this.buttonSaveKey = new System.Windows.Forms.Button();
            this.saveFileDialogImage = new System.Windows.Forms.SaveFileDialog();
            this.saveFileDialogKey = new System.Windows.Forms.SaveFileDialog();
            this.pictureBoxProgress = new System.Windows.Forms.PictureBox();
            this.labelProgress = new System.Windows.Forms.Label();
            this.labelTip = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDiffusion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProgress)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonChooseImage
            // 
            this.buttonChooseImage.Location = new System.Drawing.Point(26, 24);
            this.buttonChooseImage.Name = "buttonChooseImage";
            this.buttonChooseImage.Size = new System.Drawing.Size(113, 23);
            this.buttonChooseImage.TabIndex = 0;
            this.buttonChooseImage.Text = "Choose Image";
            this.buttonChooseImage.UseVisualStyleBackColor = true;
            this.buttonChooseImage.Click += new System.EventHandler(this.buttonChooseImage_Click);
            // 
            // checkBoxForce
            // 
            this.checkBoxForce.AutoSize = true;
            this.checkBoxForce.Location = new System.Drawing.Point(26, 118);
            this.checkBoxForce.Name = "checkBoxForce";
            this.checkBoxForce.Size = new System.Drawing.Size(88, 17);
            this.checkBoxForce.TabIndex = 1;
            this.checkBoxForce.Text = "Force embed";
            this.checkBoxForce.UseVisualStyleBackColor = true;
            // 
            // trackBarDiffusion
            // 
            this.trackBarDiffusion.Location = new System.Drawing.Point(93, 67);
            this.trackBarDiffusion.Maximum = 250;
            this.trackBarDiffusion.Name = "trackBarDiffusion";
            this.trackBarDiffusion.Size = new System.Drawing.Size(153, 45);
            this.trackBarDiffusion.TabIndex = 2;
            this.trackBarDiffusion.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarDiffusion.Value = 20;
            this.trackBarDiffusion.Scroll += new System.EventHandler(this.trackBarDiffusion_Scroll);
            // 
            // radioButtonFile
            // 
            this.radioButtonFile.AutoSize = true;
            this.radioButtonFile.Location = new System.Drawing.Point(93, 168);
            this.radioButtonFile.Name = "radioButtonFile";
            this.radioButtonFile.Size = new System.Drawing.Size(74, 17);
            this.radioButtonFile.TabIndex = 4;
            this.radioButtonFile.TabStop = true;
            this.radioButtonFile.Text = "Embed file";
            this.radioButtonFile.UseVisualStyleBackColor = true;
            this.radioButtonFile.CheckedChanged += new System.EventHandler(this.radioButtonFile_CheckedChanged);
            // 
            // radioButtonText
            // 
            this.radioButtonText.AutoSize = true;
            this.radioButtonText.Location = new System.Drawing.Point(382, 168);
            this.radioButtonText.Name = "radioButtonText";
            this.radioButtonText.Size = new System.Drawing.Size(78, 17);
            this.radioButtonText.TabIndex = 5;
            this.radioButtonText.TabStop = true;
            this.radioButtonText.Text = "Embed text";
            this.radioButtonText.UseVisualStyleBackColor = true;
            this.radioButtonText.CheckedChanged += new System.EventHandler(this.radioButtonText_CheckedChanged);
            // 
            // textBoxText
            // 
            this.textBoxText.Location = new System.Drawing.Point(309, 191);
            this.textBoxText.Multiline = true;
            this.textBoxText.Name = "textBoxText";
            this.textBoxText.Size = new System.Drawing.Size(251, 126);
            this.textBoxText.TabIndex = 6;
            // 
            // buttonEmbed
            // 
            this.buttonEmbed.Location = new System.Drawing.Point(26, 365);
            this.buttonEmbed.Name = "buttonEmbed";
            this.buttonEmbed.Size = new System.Drawing.Size(349, 52);
            this.buttonEmbed.TabIndex = 7;
            this.buttonEmbed.Text = "Embed";
            this.buttonEmbed.UseVisualStyleBackColor = true;
            this.buttonEmbed.Click += new System.EventHandler(this.buttonEmbed_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 207);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Choose file non large than 1 MB";
            // 
            // textBoxDiffusion
            // 
            this.textBoxDiffusion.Location = new System.Drawing.Point(252, 67);
            this.textBoxDiffusion.Name = "textBoxDiffusion";
            this.textBoxDiffusion.ReadOnly = true;
            this.textBoxDiffusion.Size = new System.Drawing.Size(44, 20);
            this.textBoxDiffusion.TabIndex = 9;
            this.textBoxDiffusion.Text = "20";
            // 
            // buttonChooseFile
            // 
            this.buttonChooseFile.Location = new System.Drawing.Point(54, 232);
            this.buttonChooseFile.Name = "buttonChooseFile";
            this.buttonChooseFile.Size = new System.Drawing.Size(75, 23);
            this.buttonChooseFile.TabIndex = 10;
            this.buttonChooseFile.Text = "Choose File";
            this.buttonChooseFile.UseVisualStyleBackColor = true;
            this.buttonChooseFile.Click += new System.EventHandler(this.buttonChooseFile_Click);
            // 
            // labelFileName
            // 
            this.labelFileName.AutoSize = true;
            this.labelFileName.Location = new System.Drawing.Point(51, 273);
            this.labelFileName.Name = "labelFileName";
            this.labelFileName.Size = new System.Drawing.Size(0, 13);
            this.labelFileName.TabIndex = 11;
            // 
            // comboBoxEncoding
            // 
            this.comboBoxEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEncoding.FormattingEnabled = true;
            this.comboBoxEncoding.Location = new System.Drawing.Point(309, 323);
            this.comboBoxEncoding.Name = "comboBoxEncoding";
            this.comboBoxEncoding.Size = new System.Drawing.Size(121, 21);
            this.comboBoxEncoding.TabIndex = 12;
            // 
            // comboBoxStego
            // 
            this.comboBoxStego.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStego.Enabled = false;
            this.comboBoxStego.FormattingEnabled = true;
            this.comboBoxStego.Location = new System.Drawing.Point(404, 29);
            this.comboBoxStego.Name = "comboBoxStego";
            this.comboBoxStego.Size = new System.Drawing.Size(156, 21);
            this.comboBoxStego.TabIndex = 13;
            // 
            // labelImageName
            // 
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(154, 29);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(0, 13);
            this.labelImageName.TabIndex = 14;
            // 
            // labelStego
            // 
            this.labelStego.AutoSize = true;
            this.labelStego.Location = new System.Drawing.Point(317, 32);
            this.labelStego.Name = "labelStego";
            this.labelStego.Size = new System.Drawing.Size(81, 13);
            this.labelStego.TabIndex = 15;
            this.labelStego.Text = "Stego Algorithm";
            // 
            // labelHash
            // 
            this.labelHash.AutoSize = true;
            this.labelHash.Location = new System.Drawing.Point(342, 59);
            this.labelHash.Name = "labelHash";
            this.labelHash.Size = new System.Drawing.Size(56, 13);
            this.labelHash.TabIndex = 17;
            this.labelHash.Text = "Hash Sum";
            // 
            // comboBoxHashSum
            // 
            this.comboBoxHashSum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHashSum.FormattingEnabled = true;
            this.comboBoxHashSum.Location = new System.Drawing.Point(404, 56);
            this.comboBoxHashSum.Name = "comboBoxHashSum";
            this.comboBoxHashSum.Size = new System.Drawing.Size(156, 21);
            this.comboBoxHashSum.TabIndex = 16;
            // 
            // labelCompression
            // 
            this.labelCompression.AutoSize = true;
            this.labelCompression.Location = new System.Drawing.Point(331, 86);
            this.labelCompression.Name = "labelCompression";
            this.labelCompression.Size = new System.Drawing.Size(67, 13);
            this.labelCompression.TabIndex = 19;
            this.labelCompression.Text = "Compression";
            // 
            // comboBoxCompression
            // 
            this.comboBoxCompression.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCompression.FormattingEnabled = true;
            this.comboBoxCompression.Location = new System.Drawing.Point(404, 83);
            this.comboBoxCompression.Name = "comboBoxCompression";
            this.comboBoxCompression.Size = new System.Drawing.Size(156, 21);
            this.comboBoxCompression.TabIndex = 18;
            // 
            // labelEncryption
            // 
            this.labelEncryption.AutoSize = true;
            this.labelEncryption.Location = new System.Drawing.Point(341, 113);
            this.labelEncryption.Name = "labelEncryption";
            this.labelEncryption.Size = new System.Drawing.Size(57, 13);
            this.labelEncryption.TabIndex = 21;
            this.labelEncryption.Text = "Encryption";
            // 
            // comboBoxEncryption
            // 
            this.comboBoxEncryption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEncryption.FormattingEnabled = true;
            this.comboBoxEncryption.Location = new System.Drawing.Point(404, 110);
            this.comboBoxEncryption.Name = "comboBoxEncryption";
            this.comboBoxEncryption.Size = new System.Drawing.Size(156, 21);
            this.comboBoxEncryption.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(157, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Offset";
            // 
            // textBoxOffset
            // 
            this.textBoxOffset.Location = new System.Drawing.Point(198, 116);
            this.textBoxOffset.MaxLength = 10;
            this.textBoxOffset.Name = "textBoxOffset";
            this.textBoxOffset.Size = new System.Drawing.Size(98, 20);
            this.textBoxOffset.TabIndex = 23;
            this.textBoxOffset.Text = "42";
            this.textBoxOffset.TextChanged += new System.EventHandler(this.textBoxOffset_TextChanged);
            this.textBoxOffset.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxOffset_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Max Diffusion";
            // 
            // buttonSaveImage
            // 
            this.buttonSaveImage.Location = new System.Drawing.Point(27, 394);
            this.buttonSaveImage.Name = "buttonSaveImage";
            this.buttonSaveImage.Size = new System.Drawing.Size(165, 23);
            this.buttonSaveImage.TabIndex = 25;
            this.buttonSaveImage.Text = "Save Image";
            this.buttonSaveImage.UseVisualStyleBackColor = true;
            this.buttonSaveImage.Click += new System.EventHandler(this.buttonSaveImage_Click);
            // 
            // buttonSaveKey
            // 
            this.buttonSaveKey.Location = new System.Drawing.Point(214, 394);
            this.buttonSaveKey.Name = "buttonSaveKey";
            this.buttonSaveKey.Size = new System.Drawing.Size(161, 23);
            this.buttonSaveKey.TabIndex = 26;
            this.buttonSaveKey.Text = "Save Key";
            this.buttonSaveKey.UseVisualStyleBackColor = true;
            this.buttonSaveKey.Click += new System.EventHandler(this.buttonSaveKey_Click);
            // 
            // pictureBoxProgress
            // 
            this.pictureBoxProgress.Image = global::StegoGUI.Properties.Resources.Win8ProgressIndicator;
            this.pictureBoxProgress.Location = new System.Drawing.Point(381, 365);
            this.pictureBoxProgress.Name = "pictureBoxProgress";
            this.pictureBoxProgress.Size = new System.Drawing.Size(189, 23);
            this.pictureBoxProgress.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxProgress.TabIndex = 27;
            this.pictureBoxProgress.TabStop = false;
            // 
            // labelProgress
            // 
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(423, 399);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(126, 13);
            this.labelProgress.TabIndex = 28;
            this.labelProgress.Text = "Please, wait for embed ...";
            // 
            // labelTip
            // 
            this.labelTip.AutoSize = true;
            this.labelTip.Location = new System.Drawing.Point(389, 386);
            this.labelTip.Name = "labelTip";
            this.labelTip.Size = new System.Drawing.Size(181, 13);
            this.labelTip.TabIndex = 29;
            this.labelTip.Text = "Input parameters and press \"Embed\"";
            // 
            // FormEmbed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 422);
            this.Controls.Add(this.buttonEmbed);
            this.Controls.Add(this.labelTip);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.pictureBoxProgress);
            this.Controls.Add(this.buttonSaveKey);
            this.Controls.Add(this.buttonSaveImage);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxOffset);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelEncryption);
            this.Controls.Add(this.comboBoxEncryption);
            this.Controls.Add(this.labelCompression);
            this.Controls.Add(this.comboBoxCompression);
            this.Controls.Add(this.labelHash);
            this.Controls.Add(this.comboBoxHashSum);
            this.Controls.Add(this.labelStego);
            this.Controls.Add(this.labelImageName);
            this.Controls.Add(this.comboBoxStego);
            this.Controls.Add(this.comboBoxEncoding);
            this.Controls.Add(this.labelFileName);
            this.Controls.Add(this.buttonChooseFile);
            this.Controls.Add(this.textBoxDiffusion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxText);
            this.Controls.Add(this.radioButtonText);
            this.Controls.Add(this.radioButtonFile);
            this.Controls.Add(this.trackBarDiffusion);
            this.Controls.Add(this.checkBoxForce);
            this.Controls.Add(this.buttonChooseImage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormEmbed";
            this.Text = "AStego | Embed";
            this.VisibleChanged += new System.EventHandler(this.FormEmbed_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDiffusion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProgress)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonChooseImage;
        private System.Windows.Forms.CheckBox checkBoxForce;
        private System.Windows.Forms.OpenFileDialog openFileDialogImage;
        private System.Windows.Forms.TrackBar trackBarDiffusion;
        private System.Windows.Forms.RadioButton radioButtonFile;
        private System.Windows.Forms.RadioButton radioButtonText;
        private System.Windows.Forms.TextBox textBoxText;
        private System.Windows.Forms.Button buttonEmbed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxDiffusion;
        private System.Windows.Forms.Button buttonChooseFile;
        private System.Windows.Forms.Label labelFileName;
        private System.Windows.Forms.ComboBox comboBoxEncoding;
        private System.Windows.Forms.ComboBox comboBoxStego;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Label labelStego;
        private System.Windows.Forms.Label labelHash;
        private System.Windows.Forms.ComboBox comboBoxHashSum;
        private System.Windows.Forms.Label labelCompression;
        private System.Windows.Forms.ComboBox comboBoxCompression;
        private System.Windows.Forms.Label labelEncryption;
        private System.Windows.Forms.ComboBox comboBoxEncryption;
        private System.Windows.Forms.OpenFileDialog openFileDialogFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxOffset;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonSaveImage;
        private System.Windows.Forms.Button buttonSaveKey;
        private System.Windows.Forms.SaveFileDialog saveFileDialogImage;
        private System.Windows.Forms.SaveFileDialog saveFileDialogKey;
        private System.Windows.Forms.PictureBox pictureBoxProgress;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.Label labelTip;
    }
}

